<?php
require_once("/includes/header.php");

$bezorgKnop = "";
$aanmeldKnop = "";

//geen toegang voor kioskmedewerkers
if($_SESSION == NULL) {
    ?> <script> window.location.replace("index.php"); </script> <?php
}
if($_SESSION['user']['userlevel'] == 3 || $_SESSION['user']['userlevel'] == 4) {
    ?> <script> window.location.replace("index.php"); </script> <?php exit();
}
else {

// laat bezorg knop zien als userlevel 5 is (bezorger)
if($_SESSION['user']['userlevel'] == 5) {
    $bezorgKnop = "<input type='submit' name='bezorgen' value='Pakketje Bezorgen' class='btn btn-primary btn-lg'>";
}
elseif($_SESSION['user']['userlevel'] == "1") {
    $aanmeldKnop = "<input type='submit' name='aanmelden' value='Pakketje Aanmelden' class='btn btn-primary btn-lg'>";
}

if(isset($_POST["aanmelden"])) {
    ?> <script> window.location.replace("pakketAanmelden.php"); </script> <?php
}
elseif(isset($_POST["bezorgen"])) {
    ?> <script> window.location.replace("pakketOphalen.php"); </script> <?php
}
?>
<html>
    <head> </head>    
    <body>
        <form action="" method="post">
        <div class="container">
            <div class="col-md-12 text-center">
                <div id="naglowek">
            <div class="col-md-12 col-xs-12 border">
                <div class="row">
                    <h4> Maak uw keuze! </h4>
                </div>
                <br>
                <div class="row">
                  <?php print($aanmeldKnop);?>     
                </div>
                <br>
                <div class="row">
                    <?php print($bezorgKnop); ?>
                </div>
                <br>
            </div>
        </div>
                </div>
        </form>
    </body>
</html>

<?php
require_once('/includes/footer.php');
}
?>