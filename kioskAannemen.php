<?php
require_once("/includes/header.php");

if($_SESSION['user']['userlevel'] != 3) {
    ?> <script> window.location.replace("index.php"); </script> <?php
}
?>



<div class="container">

    <div class="row">

        <div class="col-md-12 col-xs-8 border">
            <?php



                //selecteerd de afhaal punt die lijkt op de naam waarmee je bent ingelogd.
                $query = 'SELECT afhaalpuntID FROM afhaalpunt WHERE afhaalpuntStation LIKE :naam AND afhaalpuntStation LIKE :achternaam';
                $db->query($query);
                $db->bind(':naam', '%' . $_SESSION['user']['vn'] . '%');
                $db->bind(':achternaam', '%' . $_SESSION['user']['an'] . '%');
                $db->execute();
                $result = $db->single();
                $afhaalpunt = $result['afhaalpuntID'];


                //als er beneden hiero op de knop is gedrukt dan word dit gedaan, ALS ER OP HET PAKKETJE IS GEDRUKT, NADAT ER OP WERKNEMER IS GEDRUKT
            if(isset($_POST['pakketID'])){

                date_default_timezone_set('Europe/Amsterdam');
                $test = date("Y-m-d H:i:s");


                $query = 'SELECT * FROM pakket WHERE pakketID = :pakketID';
                $db->query($query);
                $db->bind(':pakketID', $_POST['pakketID']);
                $db->execute();
                $pakketresult = $db->single();
                


                //als dit het eindpunt van het pakketje is gaat hij hier verder. ANDERS GEBEURD ER HELEMAAL NIETS!!!!!!
                if($pakketresult['pakketEindpunt'] == $afhaalpunt){


                    //mail naar 3de partij


                    $sql = "UPDATE pakket SET geaccepteerd = :geaccepteerd WHERE pakketID = :pakketID";
                    $db->query($sql);
                    $db->bind(':geaccepteerd', 2);
                    $db->bind(':pakketID', $_POST['pakketID']);
                    $db->execute();

                    $sql = "UPDATE pakket_werknemer SET afgifte = :afgifte WHERE pakketID = :pakketID";
                    $db->query($sql);
                    $db->bind(':afgifte', $test);
                    $db->bind(':pakketID', $_POST['pakketID']);
                    $db->execute();

                    //query die salaris ophaald
                    $db->query("SELECT * FROM werknemer WHERE werknemerID =(SELECT werknemerID FROM pakket_werknemer WHERE afgifte = :afgifte)");
                    $db->bind(':afgifte', $test);
                    $db->execute();
                    $salaris = $db->Single();

                    //query die salaris uitbetaald
                    $uitbetaling = 3; //uitbetaling uitrekenen
                    $nieuwSalaris = $salaris['werknemerSalaris'] + $uitbetaling;
                    $db->query("UPDATE werknemer SET werknemerSalaris = :salaris WHERE werknemerID = :werknemerID ");
                    $db->bind(":salaris", $nieuwSalaris);
                    $db->bind(":werknemerID", $salaris['werknemerID']);
                    $db->execute();

                    $query = 'SELECT werknemerID, werknemerPunten FROM werknemer WHERE werknemerID IN (SELECT werknemerID FROM pakket_werknemer WHERE afgifte = :afgifte AND pakketID = :pakketID)';
                    $db->query($query);
                    $db->bind(':pakketID', $_POST['pakketID']);
                    $db->bind(':afgifte', $test);
                    $db->execute();
                    $werknemerresult = $db->single();
                    $aantalpunten = ($werknemerresult['werknemerPunten'] + 3);

                    $sql = "UPDATE werknemer SET werknemerPunten = :werknemerPunten WHERE werknemerID = :werknemerID";
                    $db->query($sql);
                    $db->bind(':werknemerPunten', $aantalpunten);
                    $db->bind(':werknemerID', $werknemerresult['werknemerID']);
                    $db->execute();

                    $sql = "INSERT INTO pakket_afhaalpunt (pakketID, afhaalpuntID, opname, afgifte) VALUES (:pakketID, :afhaalpuntID, :opname, :afgifte)";
                    $db->query($sql);
                    $db->bind(':pakketID', $_POST['pakketID']);
                    $db->bind(':afhaalpuntID', $afhaalpunt);
                    $db->bind(':opname', $test);
                    $db->bind(':afgifte', "0000-00-00 00:00:00");
                    $db->execute();



                    print("<h3 style=\"color: green; text-align: center;\"><br>Het is gelukt!! <br>Er zal zo spoedig mogelijk een 3de partij bezorger worden ingeschakeld, <br>of de eigenaar van het pakketje
                                zal persoonlijk langs komen!<br><br></h3>");
                }else{
                    print("pakket heeft een nieuw begin punt en kan opnieuw worden opgehaald!");

                    $sql = "UPDATE pakket_werknemer SET afgifte = :afgifte, afgifteAfhaalpuntID = :afgiftepunt WHERE pakketID = :pakketID";
                    $db->query($sql);
                    $db->bind(':afgifte', $test);
                    $db->bind(':afgifteAfhaalpuntID', $afhaalpunt);
                    $db->bind(':pakketID', $_POST['pakketID']);
                    $db->execute();

                    //query die salaris ophaald
                    $db->query("SELECT * FROM werknemer WHERE werknemerID =(SELECT werknemerID FROM pakket_werknemer WHERE afgifte = :afgifte)");
                    $db->bind(':afgifte', $test);
                    $db->execute();
                    $salaris = $db->Single();

                    //query die salaris uitbetaald
                    $uitbetaling = 3; //uitbetaling uitrekenen
                    $nieuwSalaris = $salaris['werknemerSalaris'] + $uitbetaling;
                    $db->query("UPDATE werknemer SET werknemerSalaris = :salaris WHERE werknemerID = :werknemerID ");
                    $db->bind(":salaris", $nieuwSalaris);
                    $db->bind(":werknemerID", $salaris['werknemerID']);
                    $db->execute();

                    $sql = "UPDATE pakket SET geaccepteerd = :geaccepteerd, pakketBeginpunt = :afhaalpunt WHERE pakketID = :pakketID";
                    $db->query($sql);
                    $db->bind(':geaccepteerd', 1);
                    $db->bind(':pakketID', $_POST['pakketID']);
                    $db->bind(':pakketBeginpunt', $afhaalpunt);
                    $db->execute();

                    $sql = "INSERT INTO pakket_afhaalpunt (pakketID, afhaalpuntID, opname, afgifte) VALUES (:pakketID, :afhaalpuntID, :opname, :afgifte)";
                    $db->query($sql);
                    $db->bind(':pakketID', $_POST['pakketID']);
                    $db->bind(':afhaalpuntID', $afhaalpunt);
                    $db->bind(':opname', $test);
                    $db->bind(':afgifte', "0000-00-00 00:00:00");
                    $db->execute();
                }


                //anders dit hieronder!!!!
            }else {
                //als er op de knop werknemer is gedrukt moet hij dit doen
                if (isset($_POST['werknemer'])) {

                    $query = "SELECT * FROM pakket_werknemer w JOIN pakket p ON p.pakketID = w.pakketID  WHERE afgifteAfhaalpuntID = :beginpunt AND w.afgifte LIKE '%0000%' AND p.geaccepteerd = 1";
                    $db->query($query);
                    $db->bind(':beginpunt', $afhaalpunt);
                    $db->execute();
                    $result = $db->resultSet();


                    $query = "SELECT afhaalpuntStation FROM afhaalpunt WHERE afhaalpuntID = :beginpunt";
                    $db->query($query);
                    $db->bind(':beginpunt', $afhaalpunt);
                    $db->execute();
                    $resu = $db->single();

                    //als er geen resultaten uit de DB komen
                    if (empty($result)) {
                        print("<h3 style=\"color: red; text-align: center;\">Helaas. Er zijn geen pakketten die hier moeten zijn. <br> Neem contact op met de klantenservice als de bezorger
                        Echt claimt dat hij hier moet zijn. <br> Excuses voor het ongemak </h3>");
                    } else {
                        ?>


                        <form method="post" action="kioskAannemen.php" id="pakketten">

                        <?php
                        print("<h3 style=\"color: green; text-align: center;\">Controleer of het werknemer ID overeenkomt met wat de bezorger zegt!<br> Bij voorbaad dank<br><br>
                                Als je op de knop drukt, accepteer je het pakket automatisch!</h3>");

                        //per pakketje alle nuttige informatie laden. Als je erop klikt word het pakketID mee gegeven aan de volgende pagina
                        foreach ($result as $value) {

                            ?>
                            <div class="text-center">
                                <button type="submit" name="pakketID" value="<?php print($value['pakketID']); ?>"><br>
                                    <?php

                                    print("<br> PakketID = " . $value['pakketID'] . "<br>");
                                    print("WerknemerID = " . $value['werknemerID'] . "<br>");
                                    print("Station = " . $resu['afhaalpuntStation'] . "<br><br><br>");

                                    ?>
                                </button>
                            </div>
                            <?php
                        }
                    }


                    //anders moet hij dit gaan doen
                } else if (isset($_POST['eerstekeer'])) {


                    //alle pakketen uit de DB halen die bij deze kiosk horen, en nog niet zijn geaccepteerd
                    $query = 'SELECT * FROM pakket WHERE pakketBeginpunt = :beginpunt AND geaccepteerd = 0;';
                    $db->query($query);
                    $db->bind(':beginpunt', $afhaalpunt);
                    $db->execute();
                    $result = $db->resultSet();

                    //als er geen resultaten uit de DB komen
                    if(empty($result)){
                        print("<h3 style=\"color: red; text-align: center;\"><br>Sorry er zijn geen pakketjes gevonden! <br><br></h3>");
                    }else {
                        print("<h3 style=\"color: green; text-align: center;\">Welk pakketje is het?</h3>");
                        ?>

                        <div class="text-center">
                            <form method="post" action="kioskControleren.php" id="pakketten">
                                <?php

                                //per pakketje alle nuttige informatie laden. Als je erop klikt word het pakketID mee gegeven aan de volgende pagina
                                foreach ($result as $value) {

                                    ?>

                                    <button type="submit" name="pakketID" value="<?php print($value['pakketID']); ?>">
                                        <br>
                                        <?php

                                        print("PakketID = " . $value['pakketID'] . "<br>");
                                        print("Lengte = " . $value['pakketLengte'] . "CM<br>");
                                        print("Breedte = " . $value['pakketBreedte'] . "CM<br>");
                                        print("Hoogte = " . $value['pakketHoogte'] . "CM<br>");
                                        print("Gewicht = " . $value['pakketGewicht'] . "gram<br>");

                                        ?>

                                    </button>

                                    <?php
                                }
                                ?>
                        </div>
                        </form>

                        <?php

                    }
                }
            }

            ?>

        </div>
    </div>
</div>


<?php

require_once('/includes/footer.php');

/**
 * Created by PhpStorm.
 * User: stefa
 * Date: 4/27/2017
 * Time: 8:47 PM
 */
?>
