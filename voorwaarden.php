<?php
require_once("/includes/header.php");
if($_SESSION['user']['userlevel'] != 1) {
    ?> <script> window.location.replace("index.php"); </script> <?php
}
?>

<div class='container'>
    <div class='row'>
     <div class='col-md-12'>
        <p>OnTrack stelt zich niet persoonlijk verantwoordelijk voor de staat waarin het door de verzender geleverde pakket geleverd wordt. Voor mankementen aan het door de verzender geleverde pakket, het zij het omhulsel van het pakket in kwestie, dan wel de bezorging wordt de aan het door de verzender geleverde pakket toegekende bezorger aansprakelijk gesteld. </p>
        <p>OnTrack stelt zich niet verantwoordelijk voor de staat van de inhoud van het door OnTrack geleverde pakket, ongeacht vanaf welke originele verzender. Schade of klachten omtrent de inhoud van het door OnTrack geleverde pakket wordt bij de originele verzender verrekend, tenzij aantoonbaar is dat de bezorger van OnTrack, toegekend aan het geleverde pakket in kwestie, verantwoordelijk is voor de aangeleverde staat van de inhoud van het pakket in kwestie.</p>
        <p>Voor klachten of opmerkingen over de levering van een pakket, geleverd door OnTrack, dient u contact op te nemen met het OnTrack hoofdkantoor, postbus 9207, +31 20 660 22 93.</p>

       <br> <br> 
     </div>
    </div>
</div>


<?php
require_once('/includes/footer.php');
?>

