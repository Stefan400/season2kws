
<?php
require_once("./includes/header.php");

$DB = new db();
$queryWerk = "SELECT sollicitantVoornaam, sollicitantTussenvoegsel, sollicitantAchternaam, werknemerPunten FROM werknemer W JOIN sollicitant S ON W.sollicitantID = S.sollicitantID ORDER BY werknemerPunten DESC LIMIT 10;";
$DB->query($queryWerk);
$resultWerk = $DB->resultSet();

$queryAfze = "SELECT afzenderVoornaam, afzenderTussenvoegsel, afzenderAchternaam, afzenderPunten FROM afzender ORDER BY afzenderPunten DESC LIMIT 10;";
$DB->query($queryAfze);
$resultAfze = $DB->resultSet();

$naam1 = ($resultWerk[0]['sollicitantVoornaam'] . " " . $resultWerk[0]['sollicitantTussenvoegsel'] . " " . $resultWerk[0]['sollicitantAchternaam']);
$naam2 = ($resultWerk[1]['sollicitantVoornaam'] . " " . $resultWerk[1]['sollicitantTussenvoegsel'] . " " . $resultWerk[1]['sollicitantAchternaam']);
$naam3 = ($resultWerk[2]['sollicitantVoornaam'] . " " . $resultWerk[2]['sollicitantTussenvoegsel'] . " " . $resultWerk[2]['sollicitantAchternaam']);

$Punt1 = ($resultWerk[0]['werknemerPunten']);
$punt2 = ($resultWerk[1]['werknemerPunten']);
$punt3 = ($resultWerk[2]['werknemerPunten']);


    
// print_r($resultWerk);   

?>
<div class="container"> 
    <div class="row ">
        <div class="col-md-4">
            <div Style="border: 2px solid green; border-radius: 25px; height: 100%; width: 100%; text-align: center;">
                <img src="/season2/images/Silver_Medal_PNG_Clip_Art.png" Style="width: 14%; height: 14%;"></img>
                <br><br>
                <p Style="font-family:verdana; font-size:170%;">
                    <?php
                    print($naam2);
                    ?>
                </p><br>
                <p Style="font-family:verdana; font-size:150%;">
                    <?php
                    print("Aantal OnPoints: " . $punt2);
                    ?>
                </p>
            </div>
        </div>
        <div class="col-md-4">
            <div Style="border: 2px solid green; border-radius: 25px; height: 100%; width: 100%; text-align: center;">
                <img src="/season2/images/Gold_Medal_PNG_Clip_Art.png" Style="width: 20%; height: 20%;"></img>
                <br><br>
                <p Style="font-family:verdana; font-size:200%;">
                    <?php
                    print($naam1);
                    ?>
                </p><br>
                <p Style="font-family:verdana; font-size:150%;">
                    <?php
                    print("Aantal OnPoints: " . $Punt1);
                    ?>
                </p>
            </div>
        </div>
        <div class="col-md-4">
            <div Style="border: 2px solid green; border-radius: 25px; height: 100%; width: 100%; text-align: center;">
                <img src="/season2/images/Bronze_Medal_PNG_Clip_art.jpg" Style="width: 12%; height: 12%;"></img>
                <br><br>
                <p Style="font-family:verdana; font-size:150%;">
                    <?php
                    print($naam3);
                    ?>
                </p><br>
                <p Style="font-family:verdana; font-size:150%;">
                    <?php
                    print("Aantal OnPoints: " . $punt3);
                    ?>
                </p>
            </div>
        </div>
        
        
        
    </div><br><br>
    
    
    <div class="col-md-12 thumbnail">    
        <div class="col-md-5">
            <p>Bezorgers</p>
            <table style="width:100%; border:1px solid #05AE0E;">
                <tr>
                    <th>Nummer</th>
                    <th>Punten</th>
                    <th>Naam</th>
                </tr>
                <tr>
                <?php
                    for ($x = 1; $x <= count($resultWerk); $x++) {
                    print("<td>" . $x . "</td>");
                    print("<td>" . $resultWerk[($x - 1)]['werknemerPunten'] . "</td>");
                    print("<td>" . $resultWerk[($x - 1)]['sollicitantVoornaam'] . " " . $resultWerk[($x - 1)]['sollicitantTussenvoegsel'] . " " . $resultWerk[($x - 1)]['sollicitantAchternaam'] . " " . "</tr>");
                    }
                ?>
                </tr>
            </table>
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-5">
            <p>Afzenders</p>
            <table style="width:100%; border:1px solid #05AE0E;">
                <tr>
                    <th>Nummer</th>
                    <th>Punten</th>
                    <th>Naam</th>
                </tr>
                <tr>
                <?php
                    for ($x = 1; $x <= count($resultAfze); $x++) {
                    print("<td>" . $x . "</td>");
                    print("<td>" . $resultAfze[($x - 1)]['afzenderPunten'] . "</td>");
                    print("<td>" . $resultAfze[($x - 1)]['afzenderVoornaam'] . " " . $resultAfze[($x - 1)]['afzenderTussenvoegsel'] . " " . $resultAfze[($x - 1)]['afzenderAchternaam'] . " " . "</tr>");
                    }
                ?>
                </tr>
            </table>
        </div>
            
    </div>       
        
</div>


<?php
require_once("./includes/footer.php");
?>