<?php
    session_start();
    include "/classes/db.class.php";
require_once("/includes/password.php");
        
    $DB = new DB();
    $username = $_POST['username'];
    $password = $_POST['password'];
    
    $queryAcc = "SELECT * FROM account WHERE accountGebruikersnaam = :un";
    $DB->query($queryAcc);
    $DB->bind(":un", $username);
    $resultAcc = $DB->single();
    
    $afzenderID = $resultAcc['afzenderID'];
    $WerknemerID = $resultAcc['werknemerID'];
    
    $queryAfz = "SELECT * FROM afzender WHERE afzenderID = :ai";
    $DB->query($queryAfz);
    $DB->bind(":ai", $afzenderID);
    $resultAfz = $DB->single();
    
    $queryWer = "SELECT * FROM werknemer W JOIN sollicitant S ON W.sollicitantID = S.sollicitantID WHERE W.werknemerID = :wi";
    $DB->query($queryWer);
    $DB->bind("wi", $WerknemerID);
    $resultWer = $DB->single();
       
    
    
    /* userlevels 
     * 1= gebruiker 
     * 2= bezorger
     * 3= Kiosk medewerker
     * 4 = HR Medewerker */
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT, ['salt' => $resultAcc['accountSalt']]); //hashed het wachtwoord.

    $inlogpogingen = $resultAcc['accountLoginpogingen'];
    
    
    //Nakijken of wachtwoord gelijk is, zoja maak standaard sessions.
    if($password == ($resultAcc['accountWachtwoord'])) {
        //print("Geautoriseerd");
        $_SESSION['authorized'] = 1;
        $_SESSION['user']['login'] = $username;
        $_SESSION['user']['wachtwoord'] = $password;
        $_SESSION['user']['userlevel'] = $resultAcc['accountUserlevel'];
        
        
        //sessions voor Afzenders / normale gebruikers
        if ($resultAcc['accountUserlevel'] == 1 && !isset($WerknemerID)) {
            $_SESSION['user']['ID'] = $resultAfz['afzenderID'];
            $_SESSION['user']['ah'] = $resultAfz['afzenderAanhef'];
            $_SESSION['user']['vn'] = $resultAfz['afzenderVoornaam'];
            $_SESSION['user']['tv'] = $resultAfz['afzenderTussenvoegsel'];
            $_SESSION['user']['an'] = $resultAfz['afzenderAchternaam'];
            $_SESSION['user']['tn'] = $resultAfz['afzenderTelefoonnummer'];
            $_SESSION['user']['em'] = $resultAfz['afzenderEmail'];
            $_SESSION['user']['ad'] = $resultAfz['afzenderAdres'];
            $_SESSION['user']['pc'] = $resultAfz['afzenderPostcode'];
            $_SESSION['user']['wp'] = $resultAfz['afzenderWoonplaats'];
            $_SESSION['user']['bd'] = $resultAfz['afzenderBedrijf'];
            $_SESSION['user']['ap'] = $resultAfz['afzenderPunten'];
        }
      //  } elseif ($resultAcc['accountUserlevel'] == 2) {
            else {
            $_SESSION['user']['wID'] = $resultWer['werknemerID'];
            $_SESSION['user']['sID'] = $resultWer['sollicitantID'];
            $_SESSION['user']['fID'] = $resultWer['functieID'];
            $_SESSION['user']['Sa'] = $resultWer['werknemerSalaris'];
            $_SESSION['user']['wp'] = $resultWer['werknemerPunten'];
            $_SESSION['user']['ah'] = $resultWer['sollicitantAanhef'];
            $_SESSION['user']['vn'] = $resultWer['sollicitantVoornaam'];
            $_SESSION['user']['tv'] = $resultWer['sollicitantTussenvoegsel'];
            $_SESSION['user']['an'] = $resultWer['sollicitantAchternaam'];
            $_SESSION['user']['tn'] = $resultWer['sollicitantTelefoonnummer'];
            $_SESSION['user']['em'] = $resultWer['sollicitantEmail'];
            $_SESSION['user']['ad'] = $resultWer['sollicitantAdres'];
            $_SESSION['user']['pc'] = $resultWer['sollicitantPostcode'];
            $_SESSION['user']['wp'] = $resultWer['sollicitantWoonplaats'];
            $_SESSION['user']['no'] = $resultWer['sollicitantNotities'];
           //$_SESSION['user']['pdf'] = $resultWer['sollicitantPDF'];
            
        }
            //refresh pagina met ingelogd account
            echo("<script>window.onload = function(){location.href = 'index.php'};</script>");
        
        }
        
    

        // Zoniet, geef melding en en return naar pagina
         else if($inlogpogingen >= 5){
             $_SESSION['unauthorized'] = 1;
            echo("<script>alert('Je account is geblokkeerd! Neem contact met ons op om je account te laten deblokkeren');</script>");
            echo("<script>window.onload = function(){location.href = 'index.php'};</script>");
            
             
         } else {
             
        $Queryinlog ="UPDATE account SET accountLoginpogingen = accountLoginpogingen + 1 WHERE accountGebruikersnaam = :gn";
        $DB->query($Queryinlog);
        $DB->bind(":gn", $username);
        $DB->execute();
        
        //print("Niet geautoriseerd");
        $_SESSION['unauthorized'] = 1;
        echo("<script>alert('Foute gegevens ingevuld');</script>");
        echo("<script>window.onload = function(){location.href = 'index.php'};</script>");
        
        
    }
    

?>