<?php
require_once("/includes/header.php");
?>
    <div class="container">
        <div class="col-md-2 artikel">
            <?php
            $query = 'SELECT * FROM afhaalpunt';
            $db->query($query);
            $db->execute();
            $result = $db->resultSet();
            print("<br>Op dit moment opereren wij in " . count($result) . " stations! <br> Hier heb je een lijst van al onze stations: <br><br>");
            $counter = 0;
            foreach ($result as $value) {
                $counter++;
                print($counter . " " . $value['afhaalpuntStation'] . "<br>");

            }
            ?>
        </div>
        <div class="col-md-10 artikel">
            <div class="padding">
                <div id="map" class="col-md-12" style="height:500px;">
                    <script>
                        var map;
                        function initMap() {
                            $.ajax({
                                url: 'getKiosken.php'
                            }).then(function (kiosken) {
                                var myLatLng = {lat: 1, lng: 2};
                                var myLatLng2 = {lat: 52.15489, lng: 4.72082};
                                map = new google.maps.Map(document.getElementById('map'), {
                                    center: myLatLng2,
                                    zoom: 9
                                });
                                for (var i = 0; i < kiosken.length; i++) {
                                    var naam = 'Station ' + kiosken[i][0];
                                    var marker = {lat: kiosken[i][1], lng: kiosken[i][2]};
                                    var marker2 = new google.maps.Marker({
                                        position: marker,
                                        map: map,
                                        title: naam

                                    });


                                }

                            })
                        }
                    </script>
                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1TO6F37aQF9pxWDCbhu8ywPg5CmSO3Pk&callback=initMap"
                            async defer></script>
                </div>
            </div>
        </div>
    </div>
    <p/>

<?php
require_once('/includes/footer.php');
?>