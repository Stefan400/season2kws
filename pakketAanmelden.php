<?php
require_once("/includes/header.php");
$userlevel = $_SESSION['user']['userlevel']; 
if($userlevel == "1") {
   
//fucties
function required($input)
{
    global $error1;
    if (isset($_POST['next']) && empty($_POST[$input])) {
        $error1 = true;
        return ' style="color:red;"';
    }
}
 $error = "";   
$db = new DB();

if(isset($_POST["submit"])) {
   if(!empty($_POST["aanhef"]) && !empty($_POST["voornaam"]) && !empty($_POST["achternaam"]) && !empty($_POST["adres"]) && !empty($_POST["postcode"]) && !empty($_POST["woonplaats"]) && !empty($_POST["lengte"]) && !empty($_POST["breedte"]) && !empty($_POST["hoogte"]) && !empty($_POST["gewicht"]) && !empty($_POST["beginstation"]) && !empty($_POST["eindstation"]) ) {
    
    
    //Page 1: Ontvanger
    $ontvangerAanhef        = $_POST['aanhef'];
    $ontvangerVoornaam      = $_POST['voornaam'];
    $ontvangerTussenvoegsel = $_POST['tussenvoegsel'];
    $ontvangerAchternaam    = $_POST['achternaam'];
    $ontvangerAdres         = $_POST['adres'];
    $ontvangerPostcode      = $_POST['postcode'];
    $ontvangerWoonplaats    = $_POST['woonplaats'];
    
    //Page 2: Pakketgrootte
    $pakketLengte           = $_POST['lengte'];
    $pakketBreedte          = $_POST['breedte'];
    $pakketHoogte           = $_POST['hoogte'];
    $pakketGewicht          = $_POST['gewicht'];
    $pakketOpmerkingen      = $_POST['opmerkingen'];
    
    //Page 3: Van?
    $vanStation             = $_POST['beginstation'];
    $naarStation            = $_POST['eindstation'];
    
    $vanHuis                = $_POST['vanhuis'];
    $aanHuis                = $_POST['aanhuis'];
    
    $vanhuisAdres           = $_POST['vanhuisAdres'];
    $vanhuisPostcode        = $_POST['vanhuisPostcode'];
    $vanhuisWoonplaats      = $_POST['vanhuisWoonplaats'];
    
    $aanhuisAdres           = $_POST['aanhuisAdres'];
    $aanhuisPostcode        = $_POST['aanhuisPostcode'];
    $aanhuisWoonplaats      = $_POST['aanhuisWoonplaats'];
    
    $userID                 = $_SESSION['user']['ID'];
    
    if(isset($aanhuisAdres) && $aanhuisAdres != "" 
    && isset($aanhuisPostcode) && $aanhuisPostcode != ""
    && isset($aanhuisWoonplaats) && $aanhuisWoonplaats != ""){
        $ontvangerAdres = $aanhuisAdres;
        $ontvangerPostcode = $aanhuisPostcode;
        $ontvangerWoonplaats = $aanhuisWoonplaats;
    }
    $db->query("SELECT * FROM ontvanger WHERE ontvangerAanhef = '$ontvangerAanhef' "
                                     . "AND ontvangerVoornaam = '$ontvangerVoornaam' "
                                     . "AND ontvangerTussenvoegsel = '$ontvangerTussenvoegsel' "
                                     . "AND ontvangerAchternaam = '$ontvangerAchternaam' "
                                     . "AND ontvangerAdres = '$ontvangerAdres' "
                                     . "AND ontvangerPostcode = '$ontvangerPostcode' "
                                     . "AND ontvangerWoonplaats = '$ontvangerWoonplaats' "
                                     . "AND afzenderID = $userID");
    $ontvanger = $db->single();
    
    if($ontvanger['ontvangerVoornaam'] == "" || $ontvanger['ontvangerVoornaam'] == null || !isset($ontvanger['ontvangerVoornaam'])){
        $db->query("INSERT INTO ontvanger(ontvangerAanhef, ontvangerVoornaam, ontvangerTussenvoegsel, ontvangerAchternaam, ontvangerAdres, ontvangerPostcode, ontvangerWoonplaats, afzenderID)"
                   . "VALUES (:ah, :vn, :tv, :an, :ad, :pc, :wp, :afid)");
        $db->bind(":ah", $ontvangerAanhef);
        $db->bind(":vn", $ontvangerVoornaam);
        $db->bind(":tv", $ontvangerTussenvoegsel);
        $db->bind(":an", $ontvangerAchternaam);
        $db->bind(":ad", $ontvangerAdres);
        $db->bind(":pc", $ontvangerPostcode);
        $db->bind(":wp", $ontvangerWoonplaats);
        $db->bind(":afid", $_SESSION['user']['ID']);
        $db->execute();
    }
    
    $db->query("SELECT ontvangerID FROM ontvanger WHERE ontvangerAanhef = '$ontvangerAanhef' "
                                     . "AND ontvangerVoornaam = '$ontvangerVoornaam' "
                                     . "AND ontvangerTussenvoegsel = '$ontvangerTussenvoegsel' "
                                     . "AND ontvangerAchternaam = '$ontvangerAchternaam' "
                                     . "AND ontvangerAdres = '$ontvangerAdres' "
                                     . "AND ontvangerPostcode = '$ontvangerPostcode' "
                                     . "AND ontvangerWoonplaats = '$ontvangerWoonplaats' "
                                     . "AND afzenderID = $userID");
    $ontvangerIDPre = $db->single();
    $ontvangerID = $ontvangerIDPre["ontvangerID"];
    
    $db->query("INSERT INTO pakket(pakketLengte, pakketBreedte, pakketHoogte, pakketGewicht, afzenderID, pakketVerzenddatum, pakketBeginpunt, pakketEindpunt, pakketOphalenVanHuis, pakketBrengenNaarHuis, ontvangerID, geaccepteerd, pakketOpmerkingen)"
            . " VALUES(:pl, :pb, :ph, :pg, :aid, :pvd, :pbegin, :peind, :pophalen, :pbezorgen, :ontvID, :accept, :opm)");
    $db->bind(":pl", $pakketLengte);
    $db->bind(":pb", $pakketBreedte);
    $db->bind(":ph", $pakketHoogte);
    $db->bind(":pg", $pakketGewicht);
    $db->bind(":aid", $userID);
    date_default_timezone_set("Europe/Amsterdam");
    $time = date("Y-m-d H:i:s", time());
    $db->bind(":pvd", $time);
    $db->bind(":pbegin", $vanStation);
    $db->bind(":peind", $naarStation);
    $db->bind(":pophalen", $vanHuis);
    $db->bind(":pbezorgen", $aanHuis);
    $db->bind(":ontvID", $ontvangerID);
    $db->bind(":accept", 0);
    $db->bind(":opm", $pakketOpmerkingen);
    $db->execute();
    
    // Select de net ingevoerde pakket van hierboven voor het pakket id
    $db->query("SELECT pakketID, pakketBeginpunt FROM pakket WHERE pakketLengte = $pakketLengte AND pakketBreedte = $pakketBreedte AND pakketGewicht = $pakketGewicht AND afzenderID = $userID AND ontvangerID = $ontvangerID AND pakketVerzenddatum = '$time'");
    $db->execute();
    $result = $db->resultSet();
    foreach($result as $value) {
        $id = $value["pakketID"];
        $afhaalpunt = $value["pakketBeginpunt"];
    }
    
    //insert into pakket_afhaalpunt
    $db->query("INSERT INTO pakket_afhaalpunt (pakketID, afhaalpuntID, opname, afgifte VALUES(:id, :afhaalpunt, :opname, :afgifte)");
    $db->bind(":id", $id);
    $db->bind(":afhaalpunt", $afhaalpunt);
    $db->bind(":opname", date("Y-m-d H:i:s"));
    $db->bind(":afgifte", "0000-00-00 00:00:00");
    
    //OnPoints ophalen van afzender
    $sql = "SELECT afzenderPunten FROM afzender WHERE afzenderID = :aID";
    $db->query($sql);
    $db->bind(":aID", $userID);
    $db->execute();
    $resultPoints = $db->single();
    
    //OnPoints geven aan afzender
    $newPoints = $resultPoints['afzenderPunten'] + 3;
    $sql = "UPDATE afzender SET afzenderPunten = :ap WHERE afzenderID = :aID";
    $db->query($sql);
    $db->bind(":ap", $newPoints);
    $db->bind(":aID", $userID);
    $db->execute();  
    
    // Zorgen dat pakket in pakket_derdepartij tabel komt
    $db->query("INSERT INTO pakket_derdepartij (pakketID, derdepartijID, opname) VALUES (:pID, :dpID, :opname)");
    $db->bind(":pID", $id);
    $db->bind(":dpID", 1);
    $db->bind(":opname", "0000-00-00 00:00:00");
    $db->execute();
    } 
    else {
        ?> <script> alert("U heeft niet alle verplichte gegevens ingevuld"); </script> <?php
        $error = "U heeft niet alle verplichte gegevens ingevuld";
    }
}

?>
    <link rel="stylesheet" href="/season2/styling/css/pakket.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript">
        function scrollTo() {
            $('html, body').animate({ scrollTop: $('#navigatorScroll-1').offset().top }, 'slow');
            return false;
        }
        
        function scrollTo2() {
            $('html, body').animate({ scrollTop: $('#navigatorScroll-2').offset().top }, 'slow');
            return false;
        }
        
        function scrollTo3(){
            $('html, body').animate({ scrollTop: $('#navigatorScroll-3').offset().top }, 'slow');
            return false;
        }
    </script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
    //Distance Matrix handler
    function calcDistance(){
        var vanhuisElem = document.getElementsByName("vanhuis")[0].value;
        var aanhuisElem = document.getElementsByName("aanhuis")[0].value;
        
        if(aanhuisElem == 1 || aanhuisElem == "1"){
            var adres = document.getElementsByName("aanhuisAdres")[0].value;
            var woonplaats = document.getElementsByName("aanhuisWoonplaats")[0].value;
            
            var heen = (adres + "+" + woonplaats).replace(" ", "+");
        }
        
        if(vanhuisElem == 1 || vanhuisElem == "1"){
            var adresEigen = document.getElementsByName("vanhuisAdres")[0].value;
            var woonplaatsEigen = document.getElementsByName("vanhuisWoonplaats")[0].value;
            
            var vandaan = (adresEigen + "+" + woonplaatsEigen).replace(" ", "+");
        }
        
        if(adres != "" && woonplaats != "" && adresEigen != "" && woonplaatsEigen != ""){
            var beginstation = document.getElementsByName("beginstation")[0];
            var beginstationText = beginstation.options[beginstation.selectedIndex].text;
            var linkBS = beginstationText.replace(" ", "+");
            
            //JSON -> Hoe ver van jouw huis naar station?
            $.getJSON('https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='+vandaan+'&destinations='+linkBS+'&key=AIzaSyASt8iVWcrX_HVlrmwXFRVpqDASFEqmSCY', function(data) {
                //data = Returned JSON
                var afstand = data.rows[0].elements[0].distance.text;
                var vervoerstype = "";
                
                if(parseFloat(afstand) > 10){
                    vervoerstype = " grijs... OnTrack biedt echter zo snel mogelijk een groene optie!";
                }
                else{
                    vervoerstype = " groen! Je maakt de wereld zo een stukje beter!";
                }
                
                document.getElementById("afstandNaarStation").innerHTML = "Het is " + afstand + " naar dit station. Dit vervoeren wij " + vervoerstype;
            });
            
            var eindstation = document.getElementsByName("eindstation")[0];
            var eindstationText = eindstation.options[eindstation.selectedIndex].text;
            var linkES = eindstationText.replace(" ", "+");
            
            //JSON -> Hoe ver naar de eindbestemming?
            $.getJSON('https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='+linkES+'&destinations='+heen+'&key=AIzaSyASt8iVWcrX_HVlrmwXFRVpqDASFEqmSCY', function(data) {
                //data = Returned JSON
                var afstand = data.rows[0].elements[0].distance.text;
                var vervoerstype = "";
                
                if(parseFloat(afstand) > 10){
                    vervoerstype = " grijs... OnTrack biedt echter zo snel mogelijk een groene optie!";
                }
                else{
                    vervoerstype = " groen! Je maakt de wereld zo een stukje beter!";
                }
                
                document.getElementById("afstandVanStation").innerHTML = "Het is " + afstand + " naar de eindbestemming. Dit vervoeren wij " + vervoerstype;
            });
        }
        
    }
    </script>
</head>

<!-- Body container -->
<body>
    <div id="Container">
        <form id="pakketAanmeldformulier" method="post" action="pakketAanmelden.php">
            <div id="navigatorScroll-1">
                <div class="col-md-12 col-xs-12">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <h1>Wie krijgt je pakketje?</h1>
                        <table class="table table-responsive">
                            <tr>
                               <td  <?php print(required("aanhef"))?>>
                                    <input type="radio" name="aanhef" value="Dhr." <?php if(isset($_POST["submit"]) && !empty($_POST["aanhef"])) {if($_POST["aanhef"] == "Dhr.") {print("Checked");}} ?>/> Dhr. 
                                    <input type='radio' name='aanhef' value='Mevr.'<?php if(isset($_POST["submit"]) && !empty($_POST["aanhef"])) {if($_POST["aanhef"] == "Mevr.") {print("Checked");}} ?>/> Mevr.
                                    (verplicht)
                                </td>
                            </tr>
                            <tr>
                                <td <?php print(required("voornaam"))?>> <input class="form-control" type="text" name="voornaam" placeholder="Voornaam (verplicht)" value="<?php if(isset($_POST["submit"]) && !empty($_POST["voornaam"])) {print($_POST["voornaam"]);} ?>"/></td>
                            </tr>
                            <tr>
                                <td><input class="form-control" type="text" name="tussenvoegsel" placeholder="Tussenvoegsel" value="<?php if(isset($_POST["submit"]) && !empty($_POST["tussenvoegsel"])) {print($_POST["tussenvoegsel"]);} ?>"/></td>
                            </tr>
                            <tr>
                                <td <?php print(required("achternaam")) ?>> <input class="form-control" type="text" name="achternaam" placeholder="Achternaam (verplicht)" value="<?php if(isset($_POST["submit"]) && !empty($_POST["achternaam"])) {print($_POST["achternaam"]);} ?>"/></td>
                            </tr>
                            <tr>
                                <td <?php print(required("adres")) ?>><input class="form-control" type="text" name="adres" placeholder="Adres (verplicht)" value="<?php if(isset($_POST["submit"]) && !empty($_POST["adres"])) {print($_POST["adres"]);} ?>"/></td>
                            </tr>
                            <tr>
                                <td <?php print(required("postcode")) ?>><input class="form-control" type="text" name="postcode" placeholder="Postcode (verplicht)" maxlength="6" value="<?php if(isset($_POST["submit"]) && !empty($_POST["postcode"])) {print($_POST["postcode"]);} ?>"/></td>
                            </tr>
                            <tr>
                                <td <?php print(required("woonplaats")) ?>><input class="form-control" type="text" name="woonplaats" placeholder="Woonplaats (verplicht)" value="<?php if(isset($_POST["submit"]) && !empty($_POST["woonplaats"])) {print($_POST["woonplaats"]);} ?>"/></td>
                            </tr>
                            <tr>
                                <td><input class="btn btn-primary" type="button" value="Volgende" onclick="scrollTo2()"</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
            <div id="navigatorScroll-2">
                <div class="col-md-12">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 col-xs-12">
                        <h1>Hoe groot is je pakketje?</h1>
                        <table class="table table-responsive">
                            <tr>
                                <td <?php print(required("lengte")) ?>><input class="form-control" type="number" name="lengte" placeholder="Lengte (cm) (verplicht)" value="<?php if(isset($_POST["submit"]) && !empty($_POST["lengte"])) {print($_POST["lengte"]);} ?>"/></td>
                            </tr>
                            <tr>
                                <td <?php print(required("breedte")) ?>><input class="form-control"type="number" name="breedte" placeholder="Breedte (cm) (verplicht)" value="<?php if(isset($_POST["submit"]) && !empty($_POST["breedte"])) {print($_POST["breedte"]);} ?>"/></td>
                            </tr>
                            <tr>
                                <td <?php print(required("hoogte")) ?>><input class="form-control" type="number" name="hoogte" placeholder="Hoogte (cm) (verplicht)" value="<?php if(isset($_POST["submit"]) && !empty($_POST["hoogte"])) {print($_POST["hoogte"]);} ?>"/></td>
                            </tr>
                            <tr>
                                <td <?php print(required("gewicht")) ?>><input class="form-control" type="number" name="gewicht" placeholder="Gewicht (gram) (verplicht)" value="<?php if(isset($_POST["submit"]) && !empty($_POST["gewicht"])) {print($_POST["gewicht"]);} ?>"/></td>
                            </tr>
                            <tr>
                                <td><textarea class="form-control" name="opmerkingen" placeholder="Moeten wij ergens rekening mee houden?" rows="4" value="<?php if(isset($_POST["submit"]) && !empty($_POST["opmerkingen"])) {print($_POST["opmerkingen"]);} ?>"></textarea></td>
                            </tr>
                            <tr>
                                <td><input class="btn btn-primary" type="button" value="Volgende" onclick="scrollTo3()"/></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
            <div id="navigatorScroll-3">
                <div class="col-md-12 col-xs-12">
                    <div class="col-md-6 col-xs-12 divisionLeft">
                        <h1>Waar komt je pakketje vandaan?</h1>
                        <table class="table table-responsive">
                            <tr>
                                <td>
                                     <select class="form-control" name="beginstation" placeholder="Beginstation" onchange="calcDistance()">
                                        <?php
                                            $db->query("SELECT DISTINCT afhaalpuntStation, afhaalpuntID FROM afhaalpunt ORDER BY afhaalpuntStation ASC");
                                            $stations = $db->resultSet();
                                            foreach($stations as $station){
                                                echo "<option value='" . $station['afhaalpuntID'] . "'>" . $station['afhaalpuntStation'] . "</option>";
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        
                        <h2>Moeten we je pakketje ophalen?</h2>
                        <table class="table table-responsive">
                            <tr>
                                <td>
                                    <select class="form-control" name="vanhuis" onchange="calcDistance()">
                                        <option value="0">Nee</option>
                                        <option value="1">Ja</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Onderstaande velden alleen invullen als we jouw pakketje van huis moeten halen!</td>
                            </tr>
                            <tr>
                                <td><input class="form-control" type="text" name="vanhuisAdres" placeholder="Jouw adres" onchange="calcDistance()"/></td>
                            </tr>
                            <tr>
                                <td><input class="form-control" type="text" name="vanhuisPostcode" placeholder="Jouw postcode" onchange="calcDistance()"/></td>
                            </tr>
                            <tr>
                                <td><input class="form-control" type="text" name="vanhuisWoonplaats" placeholder="Jouw woonplaats" onchange="calcDistance()"/></td>
                            </tr>
                            <tr>
                                <td><p id="afstandNaarStation"></p></td>
                            </tr>
                        </table>
                        <table>
                            
                        </table>
                    </div>
                    <div class="col-md-6 col-xs-12 divisionRight">
                        <h1>En waar moet hij naartoe?</h1>
                        <table class="table table-responsive">
                            <select class="form-control" name="eindstation" placeholder="Eindstation" onchange="calcDistance()">
                                <?php
                                    $db->query("SELECT DISTINCT afhaalpuntStation, afhaalpuntID FROM afhaalpunt ORDER BY afhaalpuntStation ASC");
                                    $stations = $db->resultSet();
                                    foreach($stations as $station){
                                        echo "<option value='" . $station['afhaalpuntID'] . "'>" . $station['afhaalpuntStation'] . "</option>";
                                    }
                                ?>
                            </select>
                        </table>
                        <h2>Moeten we je pakketje aan huis bezorgen?</h2>
                        <table class="table table-responsive">
                            <tr>
                                <td>
                                    <select class="form-control" name="aanhuis" onchange="calcDistance()">
                                        <option value="0">Nee</option>
                                        <option value="1">Ja</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Onderstaande velden alleen invullen als de ontvanger het pakketje op een alternatieve locatie wilt ontvangen!<br></td>
                            </tr>
                            <tr>
                                <td><input class="form-control" type="text" name="aanhuisAdres" placeholder="Adres" onchange="calcDistance()"/></td>
                            </tr>
                            <tr>
                                <td><input class="form-control" type="text" name="aanhuisPostcode" placeholder="Postcode" onchange="calcDistance()"/></td>
                            </tr>
                            <tr>
                                <td><input class="form-control" type="text" name="aanhuisWoonplaats" placeholder="Woonplaats" onchange="calcDistance()"/></td>
                            </tr>
                            <tr>
                                <td><p id="afstandVanStation"></p></td>
                            </tr>
                            <tr>
                                <td>Bij het gebruik van de bezorgservice van OnTrack ga je automatisch akkoord met onze algemene voorwaarden.<br></td>
                            </tr>
                            <tr>
                                <td> <?php print($error); ?></td>
                            </tr>
                            <tr>
                                <td><input class="form-control btn btn-primary" type="submit" name="submit" value="Pakketje aanmelden!"/></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</HTML>

<?php
require_once('/includes/footer.php');
}
else {
    ?> <script> window.location.replace("index.php"); </script> <?php
}

?>
