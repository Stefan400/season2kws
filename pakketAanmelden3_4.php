<?php
require_once("/includes/header.php");

$test = "";
$melding = "";
$eindPlaatsnaam = 0;
$adresVeld = "";
$adresKnop = "";
$adresVeldOA = "";
$adresKnopOA = "";

$afzenderID = $_GET["afzID"];
$ontvangerID = $_GET["oID"];
$lengte = $_GET["l"];
$breedte = $_GET["b"];
$hoogte = $_GET["h"];
$gewicht = $_GET["g"];
$ophalen = $_GET["oh"];

//ophaal adres ophalen
$db = new db();
$query = "SELECT afzenderAdres FROM afzender WHERE afzenderID =$afzenderID";
$db->query($query);
$db->execute();
$resultAdres = $db->Single();

//bezorg adres ophalen
$sql = "SELECT ontvangerAdres FROM ontvanger WHERE ontvangerID =$ontvangerID";
$db->query($sql);
$db->execute();
$resultOntvanger = $db->single();

//ophaal adres wijzigen
if (isset($_POST["wijzig"])) {
    $adresVeld = "<input type='text' name='adres' value=''>";
    $adresKnop = "<input type='submit' name='wijzigAdres' value='Bevestigen'>";
}
if (isset($_POST["wijzigAdres"])) {
    $db = new db();
    $sql = "UPDATE afzender SET afzenderAdres = '" . $_POST["adres"] . "' WHERE afzenderID = " . $afzenderID;
    $db->query($sql);
    $db->execute();

    $adresKnop = "<A HREF='javascript:history.go(0)'><Button>Refresh</Button></A>";
}
// bezorg adres wijzigen
if (isset($_POST["wijzigOA"])) {
    $adresVeldOA = "<input type='text' name='adresOA' value=''>";
    $adresKnopOA = "<input type='submit' name='wijzigAdresOA' value='Bevestigen'>";
}
if (isset($_POST["wijzigAdresOA"])) {
    $sql = "UPDATE ontvanger SET ontvangerAdres = '" . $_POST["adresOA"] . "' WHERE ontvangerID = " . $ontvangerID;
    $db->query($sql);
    $db->execute();

    $adresKnopOA = "<A HREF='javascript:history.go(0)'><Button>Refresh</Button></A>";
}


if (isset($_POST["next"])) {
    $beginpunt = -1;
    $eindpunt = -1;

    ?>
    <script> window.location.replace("pakketAanmelden4.php?afzID=<?php print($afzenderID);?>&oID=<?php print($ontvangerID);?>&l=<?php print($lengte);?>&b=<?php print($breedte);?>&h=<?php print($hoogte);?>&g=<?php print($gewicht);?>&oh=<?php print($ophalen);?>&bp=<?php print($beginpunt);?>&ep=<?php print($eindpunt);?>"); </script> <?php
}
?>

    <header>
        <script type="text/javascript"
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzFA_P7YK6Kn3VU9vq5e9pNddsiuQPrQs&libraries=places">
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVOP8uejtKnSSvlkI0RZVyFt-zcqY4TpU&callback=initMap">
        </script>
        <script>
            function initMap() {
                var latlngString = "-33.8670522,151.1957362";
                var radius = "500";
                var type = "restaurant";
                var keyword = "cruise";

                map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 4,
                    center: {lat: -33.8670522, lng: 151.1957362}
                });
                service = new google.maps.places.PlacesService(map);
                console.log("Test");

                var latlng = "-33.8670522,151.1957362";
                var radius = "500";
                var type = "restaurant";
                var keyword = "cruise";

                var link = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latlng + "&radius=" + radius + "&type=" + type + "&keyword=" + keyword + "&key=AIzaSyBzFA_P7YK6Kn3VU9vq5e9pNddsiuQPrQs";

                $.getJSON(link, callbackFunction);

                function callbackFunction(data) {
                    console.log(data);
                }
            }
        </script>

        <input type='hidden' id='map'/>
    </header>

    <body>
    <form action="" method="post">
        <div class="container">
            <div class="col-md-12 text-center">
                <div id="naglowek">

                    <div class="col-md-6 col-xs-12 border">
                        <div class="row">
                            <h4> Ophaal adres </h4>
                        </div>
                        <div class="row">
                            <?php print($resultAdres["afzenderAdres"] . " "); ?>
                            <input type="submit" name="wijzig" value="Wijzig">
                            <br>
                            <?php print($adresVeld);
                            print($adresKnop); ?>
                        </div>
                    </div>

                    <!-- Eindstations -->
                    <div class="col-md-6 col-xs-12 border">
                        <div class="row">
                            <h4> Bezorg Adres </h4>
                        </div>
                        <div class="row">
                            <?php print($resultOntvanger["ontvangerAdres"] . " "); ?>
                            <input type="submit" name="wijzigOA" value="Wijzig">
                            <br>
                            <?php print($adresVeldOA);
                            print($adresKnopOA); ?>


                        </div>
                    </div>

                    <input type="submit" name="next" value="NEXT">
                    <input type="button" name="hoi" value="TEST" onclick=initialize()/>
                    <br> <br>
                    <?php print($melding); ?>
                </div>
            </div>
    </form>

<?php
require_once('/includes/footer.php');
?>