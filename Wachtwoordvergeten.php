
<?php
require_once("/includes/header.php");
include $_SERVER['DOCUMENT_ROOT'] . "/season2/classes/hash.class.php";
 require_once("./includes/password.php");

$error1 = FALSE;
$error2 = FALSE;


function required($input) {
    global $error1;
    if (isset($_POST['verstuur']) && empty($_POST[$input])) {
        $error1 = true;
        return ' style="color:red;"';
    }
}
?>
<div class="container">

    <div class="row">
        <div class="col-md-12">
        <div class="col-md-3"></div>
        <div class="col-md-6 col-xs-6 border text-center">
            
            <form method="post" action="Wachtwoordvergeten.php" id="Wachtwoordvergeten">    
                <table class="table">
                <tr>
                    <td <?php print(required("gebruikersnaam")) ?>>Gebruikersnaam:</td>
                    <td><input placeholder="Gebruikersnaam" inputmode="text" type="text" class="form-control" name="gebruikersnaam"></td>
                </tr>
                <tr>
                    <td <?php print(required("email")) ?>>Email:</td>
                    <td><input placeholder="Email@adres.nl" inputmode="text" type="text" class="form-control" name="email"></td>
                </tr>
                <tr>
                    <td><div class="g-recaptcha" data-sitekey="6LeEkiEUAAAAAET8JrZ_OBYL23QsP-rRiaB32LON"></div></td>
                </tr>
                <tr>
                    <td><input class="btn btn-primary" align="center" type="submit" name="verstuur" value="Verstuur"></td>
                </tr> 
                </table>
                            
            </form>


                </div>
        <div class="col-md-3"></div>
        </div>
</div>

<?php
if (isset($_POST["verstuur"])) {
    
    // Kijk eerst of de captcha aangeklikt is en  controleerd of het geen bot is
                    if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {

                        $secret = '6LeEkiEUAAAAADhoD1eiUUqaTKgF2GCgjCWJwebd';
                        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
                        $responseData = json_decode($verifyResponse);
                        // geen bot? Succes!
                        if ($responseData->success) {
                        
                            // Captcha ingevuld maar de velden niet? ERROR!
                        if ((empty($_POST['gebruikersnaam']) || empty($_POST['email'])  && isset($_POST['verzenden']))){
                                $error1 = true;
                            }
                        // Velden en Captcha goed ingevuld? Controleer de gegevens
                            
                        $gebruikersnaam = $_POST['gebruikersnaam'];
                        $DB = new DB();
                        $queryAcc = "SELECT * FROM account WHERE accountGebruikersnaam = :gn";
                        $DB->query($queryAcc);
                        $DB->bind(":gn", $gebruikersnaam);
                        $resultAcc = $DB->single();
    
                        $afzenderID = $resultAcc['afzenderID'];
                        $WerknemerID = $resultAcc['werknemerID'];

                        $queryAfz = "SELECT * FROM afzender WHERE afzenderID = :ai";
                        $DB->query($queryAfz);
                        $DB->bind(":ai", $afzenderID);
                        $resultAfz = $DB->single();

                        $queryWer = "SELECT * FROM werknemer W JOIN sollicitant S ON W.sollicitantID = S.sollicitantID WHERE W.sollicitantID = :wi";
                        $DB->query($queryWer);
                        $DB->bind("wi", $WerknemerID);
                        $resultWer = $DB->single();
                        
                        // Controleert of het Email overeenkomt met de gebruikersnaam, Zoniet? ERROR
                        if (($resultAcc['afzenderID']) == NULL){
                        if ($_POST['email'] != $resultWer['sollicitantEmail']) {
                            $error2 = TRUE;
                            }
                        } else if (($resultAcc['afzenderID']) != NULL && !isset($WerknemerID)) {                                                                                 // && !isset($WerknemerID)
                            if ($_POST['email'] != $resultAfz['afzenderEmail']) {
                            $error2 = TRUE;
                            }   
                        } else {
                            if (($_POST['email'] != $resultAfz['afzenderEmail']) && ($_POST['email'] != $resultWer['sollicitantEmail'])) {
                            $error2 = TRUE;
                            }
                        }
                        
                        
                        
                        
                        
                            //Geen errors? DOE DINGEN ENZO
                        if ($error1 == false && $error2 == false) { ?>
                            
                            <div class="alert alert-succes" role="alert">Een nieuw wachtwoord wordt verstuurd!</div>
                        <?php 
                            $WwNoSalt = substr(md5(microtime()),rand(0,26),8);
                            $hasher = new Hash(strlen($WwNoSalt) * 4);
                            $NieuwSalt = $hasher->MakeHash();
                            $NieuwWachtwoordHash = password_hash($WwNoSalt, PASSWORD_BCRYPT, ['salt' => $NieuwSalt]); //hashed het wachtwoord.
                            
                            $DBA = new DB();
                            $queryWWV = "UPDATE account SET accountWachtwoord = :nww, accountSalt = :ns WHERE accountGebruikersnaam = :gn";
                            $DBA->query($queryWWV);
                            $DBA->bind(':nww', $NieuwWachtwoordHash);
                            $DBA->bind(':ns', $NieuwSalt);
                            $DBA->bind(':gn', $gebruikersnaam);
                            $DBA->execute();
                            
                            print($WwNoSalt);
                           
                        }

                        // Zijn er errors? Print dit!
                        
                        if ($error1 == true) {
                                ?>
                                <div class="alert alert-danger" role="alert">Verplichte velden zijn niet ingevuld!</div>
                                <?php
                                $error1 = false;
                            }if ($error2 == true) {
                                ?>
                                <div class="alert alert-succes" role="alert">Een nieuw wachtwoord wordt verstuurd!</div>
                                <?php
                                $error2 = false;    
                                }
                        
                    } else {
                        ?>
                        <div class="alert alert-danger" role="alert">captcha is niet goed ingevuld!</div>
                        <?php
                    }
}
}


require_once('/includes/footer.php');

?>