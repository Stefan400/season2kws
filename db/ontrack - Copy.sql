-- phpMyAdmin SQL Dump
-- version 4.0.4.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 02, 2017 at 08:27 AM
-- Server version: 5.6.13
-- PHP Version: 5.4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ontrack`
--
CREATE DATABASE IF NOT EXISTS `ontrack` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ontrack`;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `Gebruikersnaam` varchar(64) NOT NULL,
  `Wachtwoord` varchar(255) NOT NULL,
  `Salt` varchar(32) NOT NULL,
  `User Level_UserLevelID` int(11) NOT NULL,
  PRIMARY KEY (`Gebruikersnaam`,`User Level_UserLevelID`),
  UNIQUE KEY `Wachtwoord_UNIQUE` (`Wachtwoord`),
  KEY `fk_Account_User Level1_idx` (`User Level_UserLevelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account`
--


-- --------------------------------------------------------

--
-- Table structure for table `afhaalpunt`
--

CREATE TABLE IF NOT EXISTS `afhaalpunt` (
  `AfhaalpuntID` int(11) NOT NULL AUTO_INCREMENT,
  `Station` varchar(32) NOT NULL,
  `Winkelnaam` varchar(64) NOT NULL,
  `Openingstijd` time NOT NULL,
  `Sluitingstijd` time NOT NULL,
  PRIMARY KEY (`AfhaalpuntID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `afhaalpunt`
--


-- --------------------------------------------------------

--
-- Table structure for table `afzender`
--

CREATE TABLE IF NOT EXISTS `afzender` (
  `AfzenderID` int(11) NOT NULL AUTO_INCREMENT,
  `Aanhef` varchar(6) NOT NULL,
  `Voornaam` varchar(32) NOT NULL,
  `Tussenvoegsel` varchar(6) DEFAULT NULL,
  `Achternaam` varchar(64) NOT NULL,
  `Telefoonnummer` varchar(16) NOT NULL,
  `Email` varchar(64) NOT NULL,
  `Adres` varchar(45) NOT NULL,
  `Postcode` varchar(6) NOT NULL,
  `Woonplaats` varchar(32) NOT NULL,
  `Bedrijf` varchar(32) DEFAULT NULL,
  `Geboortedatum` date NOT NULL,
  `Account_Gebruikersnaam` varchar(64) NOT NULL,
  `Account_User Level_UserLevelID1` int(11) NOT NULL,
  PRIMARY KEY (`AfzenderID`),
  KEY `fk_Afzender_Account1_idx` (`Account_Gebruikersnaam`,`Account_User Level_UserLevelID1`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `afzender`
--


-- --------------------------------------------------------

--
-- Table structure for table `derdepartij`
--

CREATE TABLE IF NOT EXISTS `derdepartij` (
  `DerdePartijID` int(11) NOT NULL AUTO_INCREMENT,
  `Contactpersoon` varchar(128) NOT NULL,
  `Email` varchar(64) NOT NULL,
  `Telefoonnummer` varchar(16) NOT NULL,
  `Vervoerstype` varchar(8) NOT NULL,
  PRIMARY KEY (`DerdePartijID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `derdepartij`
--

-- --------------------------------------------------------

--
-- Table structure for table `derdepartij_has_pakket`
--

CREATE TABLE IF NOT EXISTS `derdepartij_has_pakket` (
  `DerdePartij_DerdePartijID` int(11) NOT NULL,
  `Pakket_PakketCode` int(11) NOT NULL,
  `Opnametijd` time NOT NULL,
  PRIMARY KEY (`DerdePartij_DerdePartijID`,`Pakket_PakketCode`),
  KEY `fk_DerdePartij_has_Pakket_Pakket1_idx` (`Pakket_PakketCode`),
  KEY `fk_DerdePartij_has_Pakket_DerdePartij1_idx` (`DerdePartij_DerdePartijID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `derdepartij_has_pakket`
--

-- --------------------------------------------------------

--
-- Table structure for table `functie`
--

CREATE TABLE IF NOT EXISTS `functie` (
  `FunctieID` int(11) NOT NULL AUTO_INCREMENT,
  `Omschrijving` varchar(32) NOT NULL,
  `Werknemer_WerknemerID` int(11) NOT NULL,
  `Werknemer_Sollicitant_SollicitantID` int(11) NOT NULL,
  PRIMARY KEY (`FunctieID`),
  KEY `fk_Functie_Werknemer1_idx` (`Werknemer_WerknemerID`,`Werknemer_Sollicitant_SollicitantID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `functie`
--

-- --------------------------------------------------------

--
-- Table structure for table `pakket`
--

CREATE TABLE IF NOT EXISTS `pakket` (
  `PakketCode` int(11) NOT NULL AUTO_INCREMENT,
  `LBH` varchar(8) NOT NULL,
  `Gewicht` double NOT NULL,
  `Verzenddatum/tijd` datetime NOT NULL,
  `Beginlocatie` varchar(32) NOT NULL,
  `Eindlocatie` varchar(32) NOT NULL,
  `Afzender_AfzenderID` int(11) NOT NULL,
  PRIMARY KEY (`PakketCode`,`Afzender_AfzenderID`),
  KEY `fk_Pakket_Afzender1_idx` (`Afzender_AfzenderID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pakket`
--

-- --------------------------------------------------------

--
-- Table structure for table `pakket_has_afhaalpunt`
--

CREATE TABLE IF NOT EXISTS `pakket_has_afhaalpunt` (
  `Pakket_PakketCode` int(11) NOT NULL,
  `Afhaalpunt_AfhaalpuntID` int(11) NOT NULL,
  PRIMARY KEY (`Pakket_PakketCode`,`Afhaalpunt_AfhaalpuntID`),
  KEY `fk_Pakket_has_Afhaalpunt_Afhaalpunt1_idx` (`Afhaalpunt_AfhaalpuntID`),
  KEY `fk_Pakket_has_Afhaalpunt_Pakket1_idx` (`Pakket_PakketCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pakket_has_afhaalpunt`
--

-- --------------------------------------------------------

--
-- Table structure for table `pakket_has_werknemer`
--

CREATE TABLE IF NOT EXISTS `pakket_has_werknemer` (
  `Pakket_PakketCode` int(11) NOT NULL,
  `Werknemer_WerknemerID` int(11) NOT NULL,
  `Werknemer_Sollicitant_SollicitantID` int(11) NOT NULL,
  PRIMARY KEY (`Pakket_PakketCode`,`Werknemer_WerknemerID`,`Werknemer_Sollicitant_SollicitantID`),
  KEY `fk_Pakket_has_Werknemer_Werknemer1_idx` (`Werknemer_WerknemerID`,`Werknemer_Sollicitant_SollicitantID`),
  KEY `fk_Pakket_has_Werknemer_Pakket1_idx` (`Pakket_PakketCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pakket_has_werknemer`
--

-- --------------------------------------------------------

--
-- Table structure for table `sollicitant`
--

CREATE TABLE IF NOT EXISTS `sollicitant` (
  `SollicitantID` int(11) NOT NULL AUTO_INCREMENT,
  `Aanhef` varchar(6) NOT NULL,
  `Voornaam` varchar(32) NOT NULL,
  `Tussenvoegsel` varchar(16) DEFAULT NULL,
  `Achternaam` varchar(64) NOT NULL,
  `Telefoonnummer` varchar(16) NOT NULL,
  `Email` varchar(64) NOT NULL,
  `Adres` varchar(32) NOT NULL,
  `Postcode` varchar(6) NOT NULL,
  `Woonplaats` varchar(32) NOT NULL,
  `Geboortedatum` date NOT NULL,
  `Notities` longtext,
  `LinkCV` varchar(64) NOT NULL,
  PRIMARY KEY (`SollicitantID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sollicitant`
--


-- --------------------------------------------------------

--
-- Table structure for table `user level`
--

CREATE TABLE IF NOT EXISTS `user level` (
  `UserLevelID` int(11) NOT NULL,
  `Omschrijving` varchar(32) NOT NULL,
  PRIMARY KEY (`UserLevelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user level`
--


-- --------------------------------------------------------

--
-- Table structure for table `werknemer`
--

CREATE TABLE IF NOT EXISTS `werknemer` (
  `WerknemerID` int(11) NOT NULL AUTO_INCREMENT,
  `Sollicitant_SollicitantID` int(11) NOT NULL,
  `FunctieID` varchar(45) NOT NULL,
  `Salaris` double DEFAULT '0',
  `Account_Gebruikersnaam` varchar(64) NOT NULL,
  `Account_User Level_UserLevelID1` int(11) NOT NULL,
  PRIMARY KEY (`WerknemerID`,`Sollicitant_SollicitantID`),
  KEY `fk_Werknemer_Sollicitant_idx` (`Sollicitant_SollicitantID`),
  KEY `fk_Werknemer_Account1_idx` (`Account_Gebruikersnaam`,`Account_User Level_UserLevelID1`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `werknemer`
--

INSERT INTO `werknemer` (`WerknemerID`, `Sollicitant_SollicitantID`, `FunctieID`, `Salaris`, `Account_Gebruikersnaam`, `Account_User Level_UserLevelID1`) VALUES

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `fk_Account_User Level1` FOREIGN KEY (`User Level_UserLevelID`) REFERENCES `user level` (`UserLevelID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `afzender`
--
ALTER TABLE `afzender`
  ADD CONSTRAINT `fk_Afzender_Account1` FOREIGN KEY (`Account_Gebruikersnaam`, `Account_User Level_UserLevelID1`) REFERENCES `account` (`Gebruikersnaam`, `User Level_UserLevelID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `derdepartij_has_pakket`
--
ALTER TABLE `derdepartij_has_pakket`
  ADD CONSTRAINT `fk_DerdePartij_has_Pakket_DerdePartij1` FOREIGN KEY (`DerdePartij_DerdePartijID`) REFERENCES `derdepartij` (`DerdePartijID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_DerdePartij_has_Pakket_Pakket1` FOREIGN KEY (`Pakket_PakketCode`) REFERENCES `pakket` (`PakketCode`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `functie`
--
ALTER TABLE `functie`
  ADD CONSTRAINT `fk_Functie_Werknemer1` FOREIGN KEY (`Werknemer_WerknemerID`, `Werknemer_Sollicitant_SollicitantID`) REFERENCES `werknemer` (`WerknemerID`, `Sollicitant_SollicitantID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pakket`
--
ALTER TABLE `pakket`
  ADD CONSTRAINT `fk_Pakket_Afzender1` FOREIGN KEY (`Afzender_AfzenderID`) REFERENCES `afzender` (`AfzenderID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pakket_has_afhaalpunt`
--
ALTER TABLE `pakket_has_afhaalpunt`
  ADD CONSTRAINT `fk_Pakket_has_Afhaalpunt_Pakket1` FOREIGN KEY (`Pakket_PakketCode`) REFERENCES `pakket` (`PakketCode`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Pakket_has_Afhaalpunt_Afhaalpunt1` FOREIGN KEY (`Afhaalpunt_AfhaalpuntID`) REFERENCES `afhaalpunt` (`AfhaalpuntID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pakket_has_werknemer`
--
ALTER TABLE `pakket_has_werknemer`
  ADD CONSTRAINT `fk_Pakket_has_Werknemer_Pakket1` FOREIGN KEY (`Pakket_PakketCode`) REFERENCES `pakket` (`PakketCode`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Pakket_has_Werknemer_Werknemer1` FOREIGN KEY (`Werknemer_WerknemerID`, `Werknemer_Sollicitant_SollicitantID`) REFERENCES `werknemer` (`WerknemerID`, `Sollicitant_SollicitantID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `werknemer`
--
ALTER TABLE `werknemer`
  ADD CONSTRAINT `fk_Werknemer_Account1` FOREIGN KEY (`Account_Gebruikersnaam`, `Account_User Level_UserLevelID1`) REFERENCES `account` (`Gebruikersnaam`, `User Level_UserLevelID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Werknemer_Sollicitant` FOREIGN KEY (`Sollicitant_SollicitantID`) REFERENCES `sollicitant` (`SollicitantID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
