<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Hash{
    var $hashLength;
    var $hashContent;
    var $hashCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public function __construct($length){
        $this->hashLength = $length;
    }

    public function MakeHash(){
        $counter = 0;
        $maxLength = strlen($this->hashCharacters);
        while($counter < $this->hashLength){

            $random = rand(0,$maxLength - 1);
            $char = $this->hashCharacters[$random];
            $this->hashContent = $this->hashContent . $char;
            $counter++;
        }

        return $this->hashContent;
    }
}