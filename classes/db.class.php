<?php

/* Class: Database
 */

//Realisatie van de klasse in kwestie
//Realisatie van de daadwerkelijke klasse

class DB{
    private $host = "localhost:3307";
    private $user = "OnTrackDbHandler";
    private $pass = "Jiu,7e@P%kncaC,tO@Llvk&w/w=&wMJ`E3~zwKa5%Ce%K@FpWOf'JjoWvuoDTwaq";
    private $name = "ontrack";
        
    //Variabelen voor databasecommunicatie
    private $dbh = "";
    private $stmt = "";
    
    //Hetgeen wat wordt aangeroepen als je de DB aanmaakt, dus '= new DB();'
    public function __construct(){
        $dsn = "mysql:host=" . $this->host . ";dbname=" . $this->name;
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );
        
        try{
            //var_dump($this);
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        }
        catch(PDOException $pdoex){
            echo "Error: " . $pdoex;
            die;
        }
    }
    
    //Het omzetten van de prepared statements naar een concrete waarde. Werkt ook met enkel de eerste twee vars
    public function bind($param, $value, $type = null) {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }
    
    //Het voorbereiden van een query. Gebruik hierin Prepared Statements!!!!
    public function query($query){
        $this->stmt = $this->dbh->prepare($query);
    }
    
    //Het ophalen van een gegevensset. Komt terug als een associatieve array
    public function resultSet(){
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    //Het ophalen van een enkele rij. Komt terug als associatieve array
    public function single(){
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    //Het uitvoeren van de INSERT en UPDATE statements
    public function execute(){
        return $this->stmt->execute();
    }
    
    
}
