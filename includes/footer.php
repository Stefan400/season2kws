<?php
    $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
    $dezeLink = 'http://'.$_SERVER['HTTP_HOST']. "/season2/includes/footer.php";
        if($actual_link == $dezeLink) {
            ?> <script> window.location.replace("../index.php"); </script> <?php
        }
?>
</body>
<footer class="site-footer">
    <div class="main-footer" data-pg-collapsed>
        <div class="container">
            <div class="col-md-12">
                <div class="row thumbnail">
                    <div class="col-xs-12 col-md-3">
                        <div class="widget w-footer widget_black_studio_tinymce">
                            <h5 class="widget-title">
                            <span class="light">
                                Ranglijsten
                            </span>
                            </h5>
                            <div class="textwidget">

                                OnTrack, Beloningen met OnPoints!
                                <a href="/season2/OnPoints.php">Klik op de ranglijst.</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="widget w-footer widget_black_studio_tinymce">
                            <h5 class="widget-title">
                            <span class="light">
                                Over ons
                            </span>
                            </h5>
                            <div class="textwidget">

                                OnTrack, makers van de website
                                <a href="/season2/makers.php">Klik hier voor de makers van de website.</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="widget w-footer widget_black_studio_tinymce">
                            <h5 class="widget-title">
                            <span class="light">
                                Contact:
                            </span>
                            </h5>
                            <div class="textwidget">

                                OnTrack, op Facebook.nl? Adres? <br/>
                                <a href="/season2/Contact.php">Contactpagina</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="widget w-footer widget_black_studio_tinymce">
                            <h5 class="widget-title"><span class="light">Registreren</span> </h5>
                            <div class="textwidget">
                                
                                Wil jij op een groene manier pakketjes bezorger?
                                Of wil jij bij on komen werken als bezorger? Registreer je hier!<br/>
                                <a href="/season2/Contact.php">Registratiepagina</a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</html>