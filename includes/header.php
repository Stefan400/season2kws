<?php
session_start();
    $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
    $dezeLink = 'http://'.$_SERVER['HTTP_HOST']. "/season2/includes/header.php";
        if($actual_link == $dezeLink) {
            ?> <script> window.location.replace("../index.php"); </script> <?php
        }


//
include $_SERVER['DOCUMENT_ROOT'] . "/season2/classes/db.class.php";
//
$db = new DB();
$i = false;
$x = "sign in";
if (isset($_SESSION['authorized']) && $_SESSION['authorized'] === 1) {
    $x = "Sign Out";
    $i = true;
} else {

}


if (isset($_SESSION['unauthorized']) && $_SESSION['unauthorized'] === 1) {

    $_SESSION['unauthorized'] = NULL;
} else {

}

?>

<!DOCTYPE html>
<html>
    <head>
        
        <script src='https://www.google.com/recaptcha/api.js?hl=nl' async defer></script>
    </head>
<header>
    <?php
    require_once("./includes/headerLinks.php");
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div id="naglowek">
                    <a href="/season2/index.html">
                        <img class=".img-responsive" src="/season2/images/logo.png"
                             style="align-content: center; height: 250px;">
                    </a>
                    <h1 class="text-center">
                    </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">

        <div class="row">
            <div class="header">

                <div class="container-fluid">


                    <!-- Second navbar for sign in -->
                    <nav class="navbar navbar-default">


                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar-collapse-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="/season2/index.html">OnTrack</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="navbar-collapse-2">
                            <ul class="nav navbar-nav navbar-right">

                                <?php

                                $array = array("Index", "Over ons","OnPoints", "Makers", "Contact", "Vacature");
                                foreach ($array as $id) {
                                    ?>
                                    <li>

                                        <?php
                                        if ($id == "Index"){
                                            ?> <a href="/season2/index.html">Home</a><?php
                                        }else{
                                        ?>
                                        <a href="/season2/<?php $pageName = str_replace(" ", "", $id);
                                        $lowercaseNaam = strtolower($pageName);
                                        print ($lowercaseNaam . ".php"); ?>">
                                            <?php print$id;
                                            } ?>

                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                                <li>
                                    <a class="btn btn-default btn-outline btn-circle"
                                        <?php
                                        if ($i == true) { ?>
                                            href="logout.php" <?php

                                        } else { ?>
                                            data-toggle="collapse"
                                            href="#nav-collapse2" aria-expanded="false"
                                            aria-controls="nav-collapse2" <?php
                                        }
                                        ?>
                                    ><?php print ($x); ?>
                                    </a>
                                </li>
                                <?php
                                if ($i != true) { ?>
                                    <li>
                                        <a class="btn btn-default btn-outline btn-circle" name=registreer
                                           href="registreer.php">Registreer</a>

                                    </li>
                                    <?php
                                } else { ?>
                                    
                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Profiel
                                        <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                          <li><a href="Profiel.php">Profiel</a></li>
                                          <?php if($_SESSION['user']['userlevel'] == 5) { ?>
                                          <li><a href="navigeren.php">Navigeren</a></li>
                                          <?php }?>
                                          
                                          <?php  if($_SESSION['user']['userlevel'] == 3 || $_SESSION['user']['userlevel'] == 4) {
                                          } else {?>
                                          <li><a href="pakketten.php">Pakketten</a></li>
                                          <?php 
                                          }
                                          if($_SESSION['user']['userlevel'] == 3) {
                                              ?> <li><a href="kioskAanmelding.php">Kiosk Aanmelding</a></li> <?php
                                          }
                                          ?>
                                        </ul>
                                          <?php }?>
                            </ul>
                            <div class="collapse nav navbar-nav nav-collapse" id="nav-collapse2">

                                <form class="navbar-form navbar-center form-inline" role="form" action=login.php
                                      method="post">

                                    <div class="form-group">
                                        <label class="sr-only" for="username">Gebruikersnaam</label>
                                        <input type="text" name="username" class="form-control" id="Username"
                                               placeholder="Gebruikersnaam"
                                               autofocus required/>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="password">Wachtwoord</label>
                                        <input type="password" name="password" class="form-control" id="Password"
                                               placeholder="Wachtwoord" required/>
                                    </div>
                                    <button type="submit" class="btn btn-success">Sign in</button>
                                    <button name=cancel type="submit" class="btn btn-success" href="#nav-collapse2"
                                            data-toggle="collapse">cancel
                                    </button>
                                    <a name="wachtwoordvergeten" type="button" class="btn btn-success" href="Wachtwoordvergeten.php">Wachtwoord vergeten</a>
                                </form>
                                
                                
                            

                                <?php
                                if (isset($_POST['registreer'])) {
                                    require_once("/includes/registreer.php");
                                }
                                ?>

                            </div>
                        </div><!-- /.navbar-collapse -->
                </div><!-- /.container -->
            </div>
        </div>
    </div>
</header>
<body>
    <?php
        
        