<?php
require_once("./includes/header.php");
    if($_SESSION == NULL) {
        ?> <script> window.location.replace("index.php"); </script><?php
    }
?>
<div class="container">
    <div class="row">
        <div class="col-md-8">          
            <table class="table">
                <tr>
                    <td>Aanhef: </td>
                    <td><?php print($_SESSION['user']['ah']); ?></td>
                </tr>
                <tr>
                    <td>Voornaam:</td>
                    <td><?php print($_SESSION['user']['vn']); ?></td>
                </tr>
                <tr>
                    <td>Tussenvoegsel:</td>
                    <td><?php print($_SESSION['user']['tv']); ?></td>
                </tr>
                <tr>
                    <td>Achternaam:</td>
                    <td><?php print($_SESSION['user']['an']); ?></td>
                </tr>
                <tr>
                    <td>Telefoonnummer:</td>
                    <td><?php print($_SESSION['user']['tn']); ?></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><?php print($_SESSION['user']['em']); ?></td>
                </tr>
                <tr>
                    <td>Woonplaats:</td>
                    <td><?php print($_SESSION['user']['wp']); ?></td>
                </tr>
                <tr>
                    <td>Adres:</td>
                    <td><?php print($_SESSION['user']['ad']); ?></td>
                </tr>
                <tr>
                    <td>Postcode:</td>
                    <td><?php print($_SESSION['user']['pc']); ?></td>
                </tr>
                <?php if (isset($_SESSION['user']['bd'])) {?>
                <tr>
                    <?php
                print("<td>Bedrijfsnaam:</td>"); ?>
                <td><?php print($_SESSION['user']['bd']);} ?></td>
                </tr>
                <tr>
                    <td><a class="btn btn-primary" href="ProfielAanpassen.php">Profiel Aanpassen</a></td>
                </tr>
                
                
            </table>
        </div>    
    </div>
</div>

<?php
require_once("./includes/footer.php");
?>

