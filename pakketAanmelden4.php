<?php
require_once("/includes/header.php");


$test = "";
$melding = "";
$beginPlaatsnaam = 0;
$eindPlaatsnaam = 0;

$afzenderID = $_GET["afzID"];
$ontvangerID = $_GET["oID"];
$ophalen = $_GET["oh"];
$beginpunt = $_GET["bp"]; //LET OP! Database kan nu nog geen adressen hierin opslaan, alleen stations
$eindpunt = $_GET["ep"]; //LET OP! Database kan nu nog geen adressen hierin opslaan, alleen stations
$lengte = $_GET["l"];
$breedte = $_GET["b"];
$hoogte = $_GET["h"];
$gewicht = $_GET["g"];
date_default_timezone_set('Europe/Amsterdam');
$test = date("Y-m-d H:i:s");

if (isset($_POST["agree"])) {
// INSERT query    
    $query = 'INSERT INTO pakket
             (pakketLengte, pakketBreedte, pakketHoogte, pakketGewicht, afzenderID, pakketVerzenddatum, pakketBeginpunt, pakketEindpunt, pakketOphalenVanHuis, ontvangerID)
              VALUES (:pakketLengte, :pakketBreedte, :pakketHoogte, :pakketGewicht, :afzenderID, :aanmeldDatum, :pakketBeginpunt, :pakketEindpunt, :pakketOphalenVanHuis, :ontvangerID)';

    $db->query($query);
    $db->bind(':pakketLengte', $lengte);
    $db->bind(':pakketBreedte', $breedte);
    $db->bind(':pakketHoogte', $hoogte);
    $db->bind(':pakketGewicht', $gewicht);
    $db->bind(':afzenderID', $afzenderID);
    $db->bind(':pakketBeginpunt', $beginpunt);
    $db->bind(':pakketEindpunt', $eindpunt);
    $db->bind(':pakketOphalenVanHuis', $ophalen);
    $db->bind(':ontvangerID', $ontvangerID);
    $db->bind(':aanmeldDatum', $test);
    $db->execute();

    ?>
    <script> window.location.replace("index.php"); </script> <?php } ?>

    <form action="" method="post">
        <div class="container">
            <div class="col-md-12 text-center">
                <div id="naglowek">
                    <div class="col-md-12 col-xs-12 border">
                        <div class="row">
                            <h4> Bedankt voor het gebruiken van OnTrack! </h4>
                        </div>
                        <div class="row">
                            Door op 'Doorgaan' te klikken ga je akkoord met de <a href='voorwaarden.php'>
                                voorwaarden </a> <br> <br>
                            <input type="submit" name="agree" value="Doorgaan">
                            <br>
                            <br>

                        </div>
                    </div>
                </div>
            </div>
    </form>


<?php
require_once('/includes/footer.php');
?>