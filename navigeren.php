
<?php
require_once("/includes/header.php");


if($_SESSION['user']['userlevel'] != 5) {
    ?> <script> window.location.replace("index.php"); </script> <?php
}
?>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-8 col-xs-8 border">
                <h1>Bepaal je route!</h1>
                <p>
                    Uiteraard wil je als bezorger de snelste route hebben! Om deze
                    te beslissen, raden wij 9292 aan! Dit is een steady website die
                    onze steady dienstverlening aankan; Een perfecte combinatie dus!
                </p>
                <p>
                    Klik op de onderstaande knop om gebruik te maken van hun geweldige
                    routeberekeningen voor jouw reistraject!
                </p>
                <br>
                <div class='text-center'>
                    <a href='https://9292.nl'>
                        <image src='http://9292.nl/gimmage/N2/SixCols/Plan%20mijn%20OV-reis.png'/>
                    </a>
                </div>
            </div>
            <div class='col-md-4 col-xs-4 border thumbnail'>
                <image src='https://urbanresearch.files.wordpress.com/2008/10/cimg4039.jpg' style='width:100%; height:40%'/>
            </div>    
        </div>
    </div>
</div>
    <!-- footer -->


<?php
require_once('/includes/footer.php');

/**
 * Created by PhpStorm.
 * User: stefa
 * Date: 4/27/2017
 * Time: 8:47 PM
 */
?>