<?php
include $_SERVER['DOCUMENT_ROOT'] . "/season2/classes/hash.class.php";
//errors worden onderaan afgehandeld
$error1 = false;
$error2 = false;
$error3 = false;
$gelukt = false;

//als er op de knop is gedrukt, en 1 van deze verplichte velden is niet ingevuld, word de tekst waar dit voor staat rood weergeven
//om duidelijk te maken dat dit een verplicht veld is. Ook komt onderaan een error message getoont.
function required($input)
{
    global $error1;
    if (isset($_POST['verzenden']) && empty($_POST[$input])) {
        $error1 = true;
        return ' style="color:red;"';
    }
}

//als een van de verplichte velden niet goed is ingevuld, zorgt dit ervoor dat de waarden toch nog terug komen wat hij heeft ingevuld.
function printen($input)
{
    if (isset($_POST['verzenden']) && !empty($_POST[$input])) {
        return $_POST[$input];
    } else
        return "";
}

require_once("./includes/header.php");
require_once("./includes/password.php");
?>

    <div class="container">

    <div class="row">


        <div class="col-md-7 col-xs-4 border">

            Welkom!<br> Wat fijn dat u zich bij OnTrack wilt aansluiten. Samen zorgen wij voor een beter milieu. En dat
            kunnen wij niet zonder jou! Vul daarom de onderstaande gegevens in.
            Deze gegevens zullen wij niet aan derden versturen, en gebruiken wij alleen om het makkelijker te maken om
            pakketjes aan te melden. Na het registreren heb je de mogelijkheid om
            pakketjes te verzenden en je aan te melden als bezorger.

            <br>
            <br><br>


            <form method="post" action="registreer.php" id="registreer">
                <?php
                // als het niet gelukt is, print het form opnieuw
                if ($gelukt == false) {
                ?>

                <table>
                    <tr>
                        <td <?php print(required("radio")) ?>>Aanhef:</td>
                        <td><input align="left" type="radio" value="Dhr."
                                   name="radio" <?php if (isset($_POST['verzenden']) && !empty($_POST['radio'])) {
                                if ($_POST['radio'] == "Dhr.") {
                                    print("checked");
                                }
                            } ?>> Dhr.
                            <input align="right" type="radio" value="Mvr."
                                   name="radio" <?php if (isset($_POST['verzenden']) && !empty($_POST['radio'])) {
                                if ($_POST['radio'] == "Mvr.") {
                                    print("checked");
                                }
                            } ?>> Mvr.
                        </td>
                    </tr>
                    <tr>
                        <td <?php print(required("voornaam")) ?>>Voornaam:</td>
                        <td><input placeholder="Voornaam" inputmode="verbatim"
                                   value="<?php print(printen("voornaam")) ?>" type="text" class="form-control"
                                   name="voornaam"></td>
                    </tr>
                    <tr>
                        <td>Tussenvoegsel:</td>
                        <td><input placeholder="Tussenvoegsel" inputmode="verbatim"
                                   value="<?php print(printen("tussen")) ?>" type="text" class="form-control"
                                   name="tussen"></td>
                    </tr>
                    <tr>
                        <td <?php print(required("achternaam")) ?>>Achternaam:</td>
                        <td><input placeholder="Achternaam" value="<?php print(printen("achternaam")) ?>" type="text"
                                   class="form-control" name="achternaam"></td>
                    </tr>
                    <tr>
                        <td <?php print(required("telefoonnr")); ?>>Telefoonnummer:</td>
                        <td><input placeholder="0612345678" value="<?php print(printen("telefoonnr")) ?>" type="number"
                                   class="form-control" name="telefoonnr"</td>
                    </tr>
                    <tr>
                        <td <?php print(required("email")) ?>>E-mail:</td>
                        <td><input placeholder="E-Mail" value="<?php print(printen("email")) ?>" type="email"
                                   class="form-control" name="email"></td>
                    </tr>
                    <tr>
                        <td <?php print(required("gebruikernaam")) ?>>Gebruikernaam:</td>
                        <td><input placeholder="Gebruikernaam" value="<?php print(printen("gebruikernaam")) ?>"
                                   type="text" class="form-control" name="gebruikernaam"></td>
                    </tr>
                    <tr>
                        <td <?php print(required("wachtwoord")) ?>>Wachtwoord:*</td>
                        <td><input placeholder="wachtwoord" value="<?php print(printen("wachtwoord")) ?>"
                                   type="password" class="form-control" name="wachtwoord"></td>
                    </tr>
                    <tr>
                        <td <?php print(required("wachtwoord2")) ?>>Herhaal wachtwoord:*</td>
                        <td><input placeholder="wachtwoord" value="<?php print(printen("wachtwoord2")) ?>"
                                   type="password" class="form-control" name="wachtwoord2"></td>
                    </tr>
                    <tr>
                        <td <?php print(required("adres"));
                        print(required("adrescijfer")) ?>>Adres:
                        </td>
                        <td align="left" width=40%><input placeholder="Voorbeeldlaan"
                                                          value="<?php print(printen("adres")) ?>" type="text"
                                                          class="form-control" name="adres"></td>
                        <td align="left" width=10%><input placeholder="69"
                                                          value="<?php print(printen("adrescijfer")) ?>" type="number"
                                                          class="form-control" name="adrescijfer"></td>
                    </tr>
                    <tr>
                        <td <?php print(required("postcodecijfer"));
                        print(required("postcodeletter")) ?>>Postcode:
                        </td>
                        <td><input placeholder="1234" value="<?php print(printen("postcodecijfer")) ?>" type="number"
                                   class="form-control" name="postcodecijfer"></td>
                        <td><input placeholder="AB" value="<?php print(printen("postcodeletter")) ?>" type="text"
                                   class="form-control" name="postcodeletter"</td>
                    </tr>
                    <tr>
                        <td <?php print(required("woonplaats")) ?>>Woonplaats:</td>
                        <td><input placeholder="Woonplaats" value="<?php print(printen("woonplaats")) ?>" type="text"
                                   class="form-control" name="woonplaats"></td>
                    </tr>
                    <tr>
                        <td>Bedrijfsnaam:</td>
                        <td><input placeholder="bedrijfsnaam" value="<?php print(printen("bedrijfsnaam")) ?>"
                                   type="text" class="form-control" name="bedrijfsnaam"></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="g-recaptcha" data-sitekey="6LeEkiEUAAAAAET8JrZ_OBYL23QsP-rRiaB32LON"></div>
                        </td>
                    </tr>


                </table>
                <br>

                Opmerkingen:
                <textarea id="contactpagina" class="form-control" name="bericht" rows="5" style="width:100%;"
                          wrap="hard"><?php print(printen("bericht")) ?></textarea>
                <br>


                * Een wachtwoord moet minimaal 1 cijfer, 1 speciaal teken, en minimaal 8 karakters hebben


                <div class="col-md-9 col-xs-2border">
                </div>
                <br>
                <input class="btn btn-primary" align="center" type="submit" name="verzenden" value="verzenden">
                <br>
            </form>

            <?php
            // is er op de verzend knop geklikt? zo ja:
            if (isset($_POST['verzenden'])) {

                // Kijk eerst of de captcha aangeklikt is en  controleerd of het geen bot is
                if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {

                    $secret = '6LeEkiEUAAAAADhoD1eiUUqaTKgF2GCgjCWJwebd';
                    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);
                    $responseData = json_decode($verifyResponse);
                    // geen bot? Succes!
                    if ($responseData->success) {


                        // controleerd of verplichte velden zijn ingevoerd. zonee? ERORR
                        if ((empty($_POST['postcodecijfer']) || empty($_POST['postcodeletter']) || empty($_POST['adres']) || empty($_POST['adrescijfer'])) && isset($_POST['verzenden'])) {
                            $error2 = true;
                        }

                        // Als er geen error plaatsvind, probeer in database te gooien!
                        if (isset($_POST["verzenden"]) && $error1 == false && $error2 == false && $error3 == false) {
                            $cijfers = preg_match("#[\\1\\2\\3\\4\\6\\7\\8\\9\\0\\*\\(\\)\\_\\-\\+\\=\\{\\}\\[\\]\\|\\:\\;\\&lt;\\&gt;\\.\\?\\/\\\\\\\\]+#", $_POST['wachtwoord']);
                            $special = preg_match("#[\\~\\`\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\_\\-\\+\\=\\{\\}\\[\\]\\|\\:\\;\\&lt;\\&gt;\\.\\?\\/\\\\\\\\]+#", $_POST['wachtwoord']);
                            $query = 'SELECT afzenderEmail FROM afzender WHERE afzenderEmail = :email';
                            $db->query($query);
                            $db->bind(':email', $_POST['email']);
                            $db->execute();
                            $result = $db->single();

                            $query = 'SELECT accountGebruikersnaam FROM account WHERE accountGebruikersnaam = :accountGebruikersnaam';
                            $db->query($query);
                            $db->bind(':accountGebruikersnaam', $_POST['gebruikernaam']);
                            $db->execute();
                            $result1 = $db->single();

                            //controleerd ingevulde gegevens
                            if ($result > 0) {
                                print(" email bestaat al!!!!!!");
                            } else if ($_POST['wachtwoord'] != $_POST['wachtwoord2']) {
                                print("wachtwoorden zijn niet het zelfde!!!");
                            } else if (!$special || strlen($_POST['wachtwoord']) < 7) {
                                print("wachtwoord voldoet niet aan de eisen!!!!");
                            } else if ($result1 > 0) {
                                print("gebruikersaccount bestaat al!");
                                // geen fouten ? Gooi in database!
                            } else {


                                //password word gehasht met een random salt. De gehashte pw & salt worden vervolgens in de DB gestopt.
                                $hasher = new Hash(strlen($_POST['wachtwoord']) * 4);
                                $salt = $hasher->MakeHash();
                                $password = password_hash($_POST['wachtwoord'], PASSWORD_BCRYPT, ['salt' => $salt]); //hashed het wachtwoord.
                                $adres = ($_POST['adres'] . " " . $_POST['adrescijfer']);
                                $postcode = ($_POST['postcodecijfer'] . $_POST['postcodeletter']);
                                $query = 'INSERT INTO afzender
                                (afzenderAanhef, afzenderVoornaam, afzenderTussenvoegsel, afzenderAchternaam, afzenderTelefoonnummer, afzenderEmail, afzenderAdres, afzenderPostcode, afzenderWoonplaats, afzenderBedrijf)
                                VALUES (:Aanhef, :Voornaam, :Tussenvoegsel, :Achternaam, :Telefoonnummer, :Email, :Adres, :Postcode, :Woonplaats, :Bedrijfsnaam)
                                ';

                                $db->query($query);
                                $db->bind(':Aanhef', $_POST['radio']);
                                $db->bind(':Voornaam', $_POST['voornaam']);
                                $db->bind(':Tussenvoegsel', $_POST['tussen']);
                                $db->bind(':Achternaam', $_POST['achternaam']);
                                $db->bind(':Telefoonnummer', $_POST['telefoonnr']);
                                $db->bind(':Email', $_POST['email']);
                                $db->bind(':Adres', $adres);
                                $db->bind(':Postcode', $postcode);
                                $db->bind(':Woonplaats', $_POST['woonplaats']);
                                $db->bind(':Bedrijfsnaam', $_POST['bedrijfsnaam']);
                                $db->execute();


                                $query = 'SELECT afzenderID FROM afzender WHERE afzenderEmail = :email';
                                $db->query($query);
                                $db->bind(':email', $_POST['email']);
                                $db->execute();
                                $result = $db->single();
//                        print($result['afzenderID']);


                                $query = 'INSERT INTO account
                                  ( accountGebruikersnaam, accountWachtwoord, accountSalt, accountUserlevel, werknemerID, afzenderID, accountLoginpogingen )
                                  VALUES (:Gebruikersnaam, :Wachtwoord, :Salt, :Userlevel, :werknemerID, :AfzenderID, :poging)
                                  ';
                                $db->query($query);
                                $db->bind(':Gebruikersnaam', $_POST['gebruikernaam']);
                                $db->bind(':Wachtwoord', $password);
                                $hash = new Hash(64);
                                $db->bind(':Salt', $salt);
                                $db->bind(':Userlevel', 1);
                                $db->bind(':werknemerID', NULL);
                                $db->bind(':AfzenderID', $result['afzenderID']);
                                $db->bind(':poging', 0);
                                $db->execute();
                                $gelukt = true;
                            }
                            // wel fouten? Zoja, check welke en geef melding
                        }

                        if ($error2 == true) {
                            ?>
                            <div class="alert alert-danger" role="alert">Velden zijn incompleet</div>
                            <?php
                            $error2 = false;
                        }
                        if ($error1 == true) {
                            ?>
                            <div class="alert alert-danger" role="alert">rode velden zijn verplicht</div>
                            <?php
                            $error1 = false;
                        }
                        if ($error3 == true) {
                            ?>
                            <div class="alert alert-danger" role="alert">Velden zijn niet goed ingevuld</div>
                            <?php
                            $error3 = false;
                        }
                        // Was de captcha geen succes? Doe dit!
                    }


                    // Alles gelukt? Doe dit!
                } else {
                    ?>
                    <div class="alert alert-danger" role="alert">Captcha is niet goed ingevuld!</div>
                    <?php
                }
                if ($gelukt == true) {
                    ?>
                    <div class="alert alert-success" role="alert">Registreren is gelukt. Log nu in met uw gegevens!
                    </div>
                    <?php
                }
            }
            }
            ?>


            <br>

            <br>


        </div>
        <div class="col-md-2 col-xs-2border">


        </div>


        <div class="col-md-2 col-xs-2 col-centered">
            <!-- Placeholder text
            Wij leveren jou gegevens niet aan derden... hihihi. Echt waar!
            Wij leveren jou gegevens niet aan derden... hihihi. Echt waar!
            Wij leveren jou gegevens niet aan derden... hihihi. Echt waar!
            Wij leveren jou gegevens niet aan derden... hihihi. Echt waar!
            Wij leveren jou gegevens niet aan derden... hihihi. Echt waar!
            Wij leveren jou gegevens niet aan derden... hihihi. Echt waar!
            Wij leveren jou gegevens niet aan derden... hihihi. Echt waar!
            Wij leveren jou gegevens niet aan derden... hihihi. Echt waar!
            Wij leveren jou gegevens niet aan derden... hihihi. Echt waar!Wij leveren jou gegevens niet aan derden... hihihi. Echt waar!
            Wij leveren jou gegevens niet aan derden... hihihi. Echt waar!
            Wij leveren jou gegevens niet aan derden... hihihi. Echt waar!
            Wij leveren jou gegevens niet aan derden... hihihi. Echt waar!
            -->

        </div>
    </div>
    <!-- footer -->


<?php
require_once('/includes/footer.php');

?>