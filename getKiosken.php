<?php
/**
 * Created by PhpStorm.
 * User: Mathijs
 * Date: 16-5-2017
 * Time: 21:03
 */

include $_SERVER['DOCUMENT_ROOT'] . "/season2/classes/db.class.php";
$data = array();
$db = new DB();
$query = 'SELECT afhaalpuntStation, latitude, longitude FROM afhaalpunt';
$db->query($query);
$db->execute();
$result = $db->resultSet();
//
//
foreach($result as $value){
//    $int1 = (int)$value[1];
//    $int2 = (int)$value[2];
    array_push($data, [$value['afhaalpuntStation'], (double)$value['latitude'], (double)$value['longitude']]);
//    $temp = [$value[0], $int1, $int2];
//    array_push($data, $temp);
}
//var_dump($result, $data);
//$data = [
//    ['Rotterdam', 51.9236739, 4.4704964],
//    ['Gouda', 52.0175018310547, 4.70444440841675],
//    ['Utrecht', 52.0893191, 5.1101691],
//    ['Woerden', 52.0849990844727, 4.89361095428467],
//    ['Leiden', 52.1663486, 4.4814362],
//    ['Haarlem', 52.35203, 4.65409],
//    ['Amsterdam', 52.3791283, 4.900272],
//    ['Amsterdam', 52.3791283, 4.800272]
//];

header("Content-type: text/json");
echo json_encode($data);