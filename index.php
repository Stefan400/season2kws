<?php
require_once('./includes/header.php');

$page = (isset($_GET['page']) ? $_GET['page'] : 'index') . ".php";
if (!file_exists($page)) {
    $page = 'index.php';
}
require_once ($page);

?>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-xs-6">
                <h3>Wat is het OnTrack pakketsysteem? </h3>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/T8VS9BGj3t8?showinfo=0" frameborder="0" allowfullscreen></iframe>
                  
                </div>
        </div>
        <div class='col-md-6 col-xs-6'>
            <h1>Hallo, koele kikker!</h1>
            <p>
                Jij houdt van de planeet, net als wij!
                Wellicht ben je vegetarisch, veganistisch, biologisch of een plant
                geworden zodat je wat kan betekenen voor deze planeet, maar voelde 
                je dat er altijd wat miste in je groene hart! Wellicht 
                ben je gewoon nieuwsgierig naar OnTrack. Dat laatste moet sowieso
                waar zijn, anders was je hier natuurlijk niet!
            </p>
            <p>
                OnTrack <i>is</i> datgeen wat je groene hartje miste! De transportsector
                stoot 14% van de uitlaatgassen uit van het totaal! Dat is natuurlijk
                veel te veel! Wij van OnTrack verminderen die uitstoot door ons eigen
                ecologische steentje bij te dragen met groen transport. Zeiden we groen?
                We bedoelen natuurlijk supergroen!
            </p>
            <p>
                Jij vraagt je af hoe we dit doen. Vraag niet langer, en klik op "Over ons" voor meer info!
            </p>
        </div>    
    </div>
</div>
<?php require_once('./includes/footer.php') ?>
