
    <?php
    require_once("/includes/header.php");
    if($_SESSION['user']['userlevel'] != 3) {
        ?> <script> window.location.replace("index.php"); </script> <?php
    }
$u=0;
$error1 = "";
    ?>




<?php



date_default_timezone_set('Europe/Amsterdam');
$test = date("Y-m-d H:i:s");


$query = 'SELECT afhaalpuntID, afhaalpuntStation FROM afhaalpunt WHERE afhaalpuntStation LIKE :naam AND afhaalpuntStation LIKE :achternaam';
$db->query($query);
$db->bind(':naam', '%' . $_SESSION['user']['vn'] . '%');
$db->bind(':achternaam', '%' . $_SESSION['user']['an'] . '%');
$db->execute();
$result = $db->single();
$afhaalStation = $result['afhaalpuntStation'];
$afhaalpunt = $result['afhaalpuntID'];
?>



<div class="container">

    <div class="row">
        <div class="col-md-2 col-xs-1"></div>

        <div class="col-md-8 col-xs-8 border">


<?php
//selecteer alles van het pakket waar je net op hebt gedrukt bij kioskAannemen
$query = 'SELECT * FROM pakket WHERE pakketID = :pakketID;';
$db->query($query);
$db->bind(':pakketID', $_POST['pakketID']);
$db->execute();
$result = $db->single();
//als er nog niet op een knop is gedrukt moet hij dit laten zien
if(!isset($_POST['aanpassen_en_verzenden']) && !isset($_POST['verzenden'])){

    ?>

    <h3 style="color: green; text-align: center;">Beste kiosk medewerker,<br> Wij vragen u zorgvuldig de gegevens van het pakketje te controleren. <br>
        Dit zorgt ervoor dat het proces later soepeler verloopt.<br>
        Bij voorbaad dank!<br>
    </h3>

<!--    Voor elk veld(behalve pakketID word automatisch een invulveld gemaakt-->
            <form method="post" action="kioskControleren.php" id="pakketten">
    <table class="table">
        <tr>
            <td>pakketID:</td>
            <td><input type="hidden" name="pakketID" value="<?= $result['pakketID'] ?>"><?php print($result['pakketID']); ?></td>
        </tr>
        <tr>
            <td>Lengte:</td>
            <td><input type="text" name="lengte" placeholder="<?php print($result['pakketLengte']) ?>" value="<?php print($result['pakketLengte']) ?>">CM</td>
        </tr>
        <tr>
            <td>Breedte:</td>
            <td><input type="text" name="breedte" placeholder="<?php print($result['pakketBreedte']) ?>" value="<?php print($result['pakketBreedte']) ?>">CM</td>
        </tr>
        <tr>
            <td>Hoogte:</td>
            <td><input type="text" name="hoogte" placeholder="<?php print($result['pakketHoogte']) ?>" value="<?php print($result['pakketHoogte']) ?>">CM</td>
        </tr>
        <tr>
            <td>Gewicht:</td>
            <td><input type="text" name="gewicht" placeholder="<?php print($result['pakketGewicht']) ?>" value="<?php print($result['pakketGewicht']) ?>">gram</td>
        </tr>
        <tr>
            <td><input class="btn btn-primary" align="center" type="submit" name="aanpassen_en_verzenden" value="aanpassen en verzenden"></td>
            <td><input class="btn btn-primary" align="center" type="submit" name="verzenden" value="verzenden"></td>
        </tr>
    </table>
            </form>


    <?php
    
    
}
//als er op een van deze knoppen is gedrukt laat hij wat anders zien, en update hij de DB
if(isset($_POST['verzenden'])){
    
    
    $sql = "UPDATE pakket SET geaccepteerd = :geaccepteerd WHERE pakketID = :pakketID";
    $db->query($sql);
    $db->bind(':geaccepteerd', 1);
    $db->bind(':pakketID', $result['pakketID']);
    $db->execute();


    $sql = "INSERT INTO pakket_afhaalpunt (pakketID, afhaalpuntID, opname, afgifte) VALUES (:pakketID, :afhaalpuntID, :opname, :afgifte)";
    $db->query($sql);
    $db->bind(':pakketID', $result['pakketID']);
    $db->bind(':afhaalpuntID', $afhaalpunt);
    $db->bind(':opname', $test);
    $db->bind(':afgifte', "0000-00-00 00:00:00");
    $db->execute();
    

    ?>
    <h3 style="color: green; text-align: center;">Bedankt voor het controlleren van het pakket <br>

    </h3>
    <?php
}else if(isset($_POST['aanpassen_en_verzenden'])){
    $totaal = ($_POST['lengte'] * $_POST['breedte'] * $_POST['hoogte']);
    if($totaal > 24848 || $_POST['lengte'] > 80 || $_POST['breedte'] > 80 || $_POST['hoogte'] > 80){
        print("<h3 style=\"color: red; text-align: center;\"><br>pakketje is te groot. Pakketje mag niet groter zijn dan 80 CM <br> Neem contact op met de servicedesk <br><br></h3>");
    }else if($_POST['gewicht'] > 4000){
        print("<h3 style=\"color: red; text-align: center;\"><br>Pakket is te zwaar!<br><br></h3>");
    }else{

    $sql = "UPDATE pakket SET geaccepteerd = :geaccepteerd, pakketLengte = :lengte, pakketBreedte = :breedte, pakketHoogte = :hoogte, pakketGewicht = :gewicht  WHERE pakketID = :pakketID";
    $db->query($sql);
    $db->bind(':geaccepteerd', 1);
    $db->bind(':lengte', $_POST['lengte']);
    $db->bind(':breedte', $_POST['breedte']);
    $db->bind(':hoogte', $_POST['hoogte']);
    $db->bind(':gewicht', $_POST['gewicht']);
    $db->bind(':pakketID', $result['pakketID']);
    $db->execute();


    $sql = "INSERT INTO pakket_afhaalpunt (pakketID, afhaalpuntID, opname, afgifte) VALUES (:pakketID, :afhaalpuntID, :opname, :afgifte)";
    $db->query($sql);
    $db->bind(':pakketID', $result['pakketID']);
    $db->bind(':afhaalpuntID', $afhaalpunt);
    $db->bind(':opname', $test);
    $db->bind(':afgifte', "0000-00-00 00:00:00");
    $db->execute();



    ?>
            <h3 style="color: green; text-align: center;">Bedankt voor het aanpassen en controlleren van het pakket <br>
            </h3>

            <?php

}}

?>

        </div>
    </div>
</div>


    <?php
    require_once('/includes/footer.php');

    /**
     * Created by PhpStorm.
     * User: stefa
     * Date: 4/27/2017
     * Time: 8:47 PM
     */
    ?>
