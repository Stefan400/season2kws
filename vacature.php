

<?php
require_once("/includes/header.php");
?>
<div class="container">

    <div class="row">


        <div class="col-md-12 col-xs-12">
        <div class="col-md-8 col-xs-8 border">

            <h1>OnTrack zoekt jou!</h1>
            <h2>Waarom on track?</h2>
            <p>
                OnTrack is de nieuwste en groenste bezorgservice van Nederland!
                Wij verzekeren onze klanten om alle pakketjes op de meest groene manier
                te verzenden. Als bezorger ben jij het gezicht van het bedrijf, en
                geef je mensen een lach op hun gezicht wanneer ze het pakketje binnenkrijgen.
                Bij OnTrack ben je nooit alleen, en kom je vaak in contact met andere werknemers
                of onze partners. Ben jij sociaal en ga jij voor groen? Dan is OnTrack iets voor jou!
            </p>
            <h2>Flexibiliteit!</h2>
            <p>
                OnTrack levert de meest flexibele manier van werken die te vinden is!
                Bij OnTrack zit je niet vast aan een uren contract, maar mag je zelf bepalen
                wanneer en hoeveel je werkt. Je hoeft niets te melden als je ziek bent of
                op vakantie gaat, en je hoeft niet ingepland te worden als je wilt werken!
                Als bezorger log je simpelweg in op de site, je meldt je aan om een pakketje
                mee te nemen en klaar is kees!
                Kortom: Geld verdienen wanneer JOU dat uit komt!
            </p>
            <h2>Een groener milieu!</h2>
            <p>
                Als bezorger bij OnTrack ben jij bezig op de meest groene manier.
                Omdat jij met de trein pakketjes van station naar station brengt wordt
                er aan duizenden euro's bezine bespaart, en rijdt er een stuk minder
                onnodig grijs vervoer over de weg. Als bezorger bij OnTrack kun je geld
                verdienen terwijl je bezig bent met het verbeteren van het milieu!
                Hoe gaaf is dat!
            </p>
            <h2>Bezorger worden?</h2>
            <p>
                Aanmelden is simpel! Registreer je nu op onze site en geef op je profiel aan dat je bezorger wilt worden!
                Dan krijg je zo spoedig mogelijk een antwoord terug.
            </p>
            <p><a href="registreer.php" Style='color: #31b0d5'>Registreer je hier!</a></p>
        </div>
        
        <div class="col-md-4 col-xs-4 col-centered thumbnail">
            <image class="img-responsive" src='http://l7.alamy.com/zooms/b91075b62b1143399b7e1e4ea07f1e7d/woman-holding-packages-as-a-mailman-smiling-on-a-white-background-gj26nm.jpg'>
    </div>
</div>



<?php
require_once('/includes/footer.php');

?>