<?php
require_once("/includes/header.php");

if($_SESSION['user']['userlevel'] != 3) {
            ?> <script> window.location.replace("index.php"); </script> <?php exit();
        }
?>

<div class="container">

    <div class="row">


<!--        $_SESSION['user']['']-->
        <div class="col-md-12 col-xs-8 border">


            <?php

            date_default_timezone_set('Europe/Amsterdam');
            $test = date("Y-m-d H:i:s");


            // het afhaalpunt ID & afhaalStation waarmee is ingelogd word hier gedefineerd.
            $query = 'SELECT afhaalpuntID, afhaalpuntStation FROM afhaalpunt WHERE afhaalpuntStation LIKE :naam AND afhaalpuntStation LIKE :achternaam';
            $db->query($query);
            $db->bind(':naam', '%' . $_SESSION['user']['vn'] . '%');
            $db->bind(':achternaam', '%' . $_SESSION['user']['an'] . '%');
            $db->execute();
            $result = $db->single();
            $afhaalStation = $result['afhaalpuntStation'];
            $afhaalpunt = $result['afhaalpuntID'];


            if(isset($_POST['pakket_ontvanger'])){
                print("<h3 style=\"color: green; text-align: center;\">Je kan nu het pakketje mee geven aan de ontvanger." . "<br> <br>" .  "
                            </h3>");
                $sql = "UPDATE pakket SET geaccepteerd = :geaccepteerd WHERE pakketID = :pakketID";
                $db->query($sql);
                $db->bind(':geaccepteerd', 3);
                $db->bind(':pakketID', $_POST['pakket_ontvanger']);
                $db->execute();

                $sql = "UPDATE pakket_afhaalpunt SET afgifte = :afgifte WHERE pakketID = :pakketID AND afgifte LIKE '%0000%'";
                $db->query($sql);
                $db->bind(':afgifte', $test);
                $db->bind(':pakketID', $_POST['pakket_ontvanger']);
                $db->execute();

            }
            elseif(isset($_POST["grijsWeg"])) { // als de pakketjes zijn opgehaald door de grijze bezorger
                
                    print("Alle grijze pakketjes zijn weggegeven!");
                    
                    $sql = "SELECT * FROM pakket_afhaalpunt pa JOIN pakket p ON p.pakketID = pa.pakketID  WHERE afhaalpuntID = :afhaalpunt AND geaccepteerd <> 3"; //select de pakketjes
                    $db->query($sql);
                    $db->bind(":afhaalpunt", $afhaalpunt);
                    $db->execute();
                    $pakket = $db->resultSet();
                    
                     date_default_timezone_set('Europe/Amsterdam'); //zorgt ervoor dat er gekeken kan worden of pakket langer dan 12 uur ligt
                    $h = date("h" - 12) ;
                    $i = date("i");
                    $s = date("s");
                    $datumTijd = date("Y-m-d $h:$i:$s");
                    
                     foreach($pakket as $value) {

                        if($value['opname'] < $datumTijd) { //wat de grijze pakketjes moeten doen
                            
                            $sql = "UPDATE pakket SET geaccepteerd = :g WHERE pakketID = :pakketID";
                            $db->query($sql);
                            $db->bind(":g", 3);
                            $db->bind(":pakketID", $value['pakketID']);
                            $db->execute();
                        }
                        else {
                            print("<br>pakketje is groen <br> <br>");
                        }
                    }   
            }
            elseif(isset($_POST["haalBezorger"])) { //als grijze bezorger is gecontacteerd
                // email script om bezorger email te sturen??
                print("Bel de bezorger op 06-83349626");
            }
            
            else if(isset($_POST['pakket_meenemen'])){
                print("<h3 style=\"color: green; text-align: center;\">Je kan nu het pakketje mee geven aan de bezorger" . "<br> <br>" .  "
                            </h3>");
                $sql = "UPDATE pakket SET geaccepteerd = :geaccepteerd WHERE pakketID = :pakketID";
                $db->query($sql);
                $db->bind(':geaccepteerd', 3);
                $db->bind(':pakketID', $_POST['pakket_meenemen']);
                $db->execute();

                $sql = "UPDATE pakket_afhaalpunt SET afgifte = :afgifte WHERE pakketID = :pakketID AND afgifte LIKE '%0000%'";
                $db->query($sql);
                $db->bind(':afgifte', $test);
                $db->bind(':pakketID', $_POST['pakket_meenemen']);
                $db->execute();

                $sql = "UPDATE pakket_derdepartij SET opname = :afgifte WHERE pakketID = :pakketID";
                $db->query($sql);
                $db->bind(':afgifte', $test);
                $db->bind(':pakketID', $_POST['pakket_meenemen']);
                $db->execute();

            }else if(isset($_POST['3de_partij'])){
                $query = 'SELECT * FROM pakket_derdepartij WHERE pakketID IN (SELECT pakketID FROM pakket WHERE geaccepteerd = 2 AND pakketBrengenNaarHuis = 1 AND geaccepteerd = 2
                            AND pakketEindpunt = :eindpunt)';
                $db->query($query);
                $db->bind(':eindpunt', $afhaalpunt);
                $db->execute();
                $result = $db->resultSet();

                if($result == null){
                    print("<h3 style=\"color: red; text-align: center;\">helaas! niets gevonden." . "<br> <br>" .  "
                            </h3>");
                }else{
                    print("<h3 style=\"color: green; text-align: center;\">controlleer of zijn pakketID overeenkomt met wat hier staat." . "<br> <br>" .  "
                            </h3>");

                foreach($result as $value){
                    $query = 'SELECT * FROM derdepartij WHERE derdepartijID = :derdepartij';
                    $db->query($query);
                    $db->bind(':derdepartij', $value['derdepartijID']);
                    $db->execute();
                    $result1 = $db->single();

                    ?>

            <form method="post" action="kioskAanmelding.php" id="pakketten">
                <div  class="col-md-1 col-xs-10"></div>
                    <button type="submit" name="pakket_meenemen" value="<?php print($value['pakketID']); ?>" class="col-md-4 col-xs-10"> <br>

                        <?php
                        print("<br>pakketID = " . $value['pakketID'] . "<br>");
                        print("bedrijfsnaam = " . $result1['derdepartijBedrijf'] . "<br><br><br>");

                        ?>

                    </button>
            </form>


                    <?php
                }print("<div class=\"col-md-12 col-xs-10\"><br></div>");}


            }else if(isset($_POST['ontvanger_zelf'])){

               // $query = 'SELECT * FROM ontvanger WHERE ontvangerID IN (SELECT ontvangerID FROM pakket WHERE pakketBrengenNaarHuis = 0 AND geaccepteerd = 2 AND pakketEindpunt = :eindpunt)';
                $query = 'SELECT * FROM ontvanger o LEFT JOIN pakket p ON o.ontvangerID = p.ontvangerID WHERE  p.pakketBrengenNaarHuis = 0 AND p.geaccepteerd = 2 AND p.pakketEindpunt = :eindpunt';
                $db->query($query);
                $db->bind(':eindpunt', $afhaalpunt);
                $db->execute();
                $result = $db->resultSet();

                if($result == null){
                    print("<h3 style=\"color: red; text-align: center;\">helaas! niets gevonden." . "<br> <br>" .  "
                            </h3>");
                }else{
                    print("<h3 style=\"color: green; text-align: center;\">Wanneer je op het pakketje drukt, kan je gelijk het pakketje aan de persoon mee geven. <br> 
                        Controlleer de gegevens goed!" . "<br> <br>" .  "
                            </h3>");
                    foreach($result as $value){

                        ?>
                        <div class="col-md-1 col-xs-10"> </div>
                        <form method="post" action="kioskAanmelding.php" id="pakketten" >
                            <button type="submit" name="pakket_ontvanger" value="<?php print($value['pakketID']); ?>" class="col-md-4 col-xs-10"> <br>
                                <?php
                                print("<br>pakketID = " . $value['pakketID'] . "<br>");
                                print("Voornaam = " . $value['ontvangerVoornaam'] . "<br>");
                                print("Achternaam = " . $value['ontvangerAchternaam'] . "<br>");
                                print("adres = " . $value['ontvangerAdres'] . "<br>");


                                ?>
<br><br>
                            </button>
                        </form>
                        <div class="col-md-1 col-xs-10"> </div>

                        <?php
                    }print("<div class=\"col-md-12 col-xs-10\"><br></div>");
                }

            }else if(isset($_POST['einde'])){
            ?>
            <form method="post" action="kioskAanmelding.php" id="pakketten">
                <button type="submit" name="3de_partij" class="col-md-6 col-xs-10">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    Derde partij
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </button>


                <button type="submit" name="ontvanger_zelf" class="col-md-6 col-xs-10">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    ontvanger zelf
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </button>
            </form>
                <div class="col-md-12 col-xs-10"><br></div>
                <?php
            }else if(isset($_POST['pakketten2'])){


//TODO: Onderstaande query moet WHERE pakketID = id AND locatie = pakketlocatie
                $sql = "UPDATE pakket_werknemer SET opname = :opname WHERE pakketID = :pakketID";
                $db->query($sql);
                $db->bind(':opname', $test);
                $db->bind(':pakketID', $_POST['pakketten2']);
                $db->execute();

                $sql = "UPDATE pakket_afhaalpunt SET afgifte = :afgifte WHERE pakketID = :pakketID AND afhaalpuntID = :afhaalpuntID";
                $db->query($sql);
                $db->bind(':afgifte', $test);
                $db->bind(':pakketID', $_POST['pakketten2']);
                $db->bind(':afhaalpuntID', $afhaalpunt);
                $db->execute();

                print("<h3 style=\"color: green; text-align: center;\">Top het is helemaal gelukt. Je kan nu het bijbehorende pakketje overhandigen. <br>
                        Het pakket id is " . $_POST['pakketten2'] . "<br> <br>" .  "
                            </h3>");
            }else {


                //hij gaat alleen verder als er een user level van 3 is. user level 3 betekend kiosk medewerker
                if ($_SESSION['user']['userlevel'] == 3) {


                    //als er op geeneen knop is gedurkt doet hij dit
                    if (!isset($_POST['aannemen']) && !isset($_POST['uitgeven']) && !isset($_POST['status'])) {
                        ?>
                        <form method="post" action="kioskAanmelding.php" id="pakketten">
                            <button type="submit" name="aannemen" class="col-md-6 col-xs-10">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                Pakketje aannemen
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                            </button>


                            <button type="submit" name="uitgeven" class="col-md-6 col-xs-10">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                Pakketje uitgeven
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                            </button>


                            <button type="submit" name="status" class="col-md-6 col-xs-10">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                Pakket status
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                            </button>
                            <button type="submit" name="einde" class="col-md-6 col-xs-10">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                Pakket naar eindbezorgadres
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                            </button>


                            <br>
                            <br>


                            <br>
                            <br>
                        </form>
                        <div class="col-md-12 col-xs-10"><br></div>
                        <?php
                    }


                    if (isset($_POST['aannemen'])) {
                        ?>
                        <form method="post" action="kioskAannemen.php" id="pakketten">
                            <button type="submit" name="werknemer" class="col-md-6 col-xs-10">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                Werknemer
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                            </button>
                            <button type="submit" name="eerstekeer" class="col-md-6 col-xs-10">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                Verzender
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                            </button>

                        </form>
                        <div class="col-md-12 col-xs-10"><br></div>

                        <?php


                    }
                    elseif(isset($_POST['status'])) {
                       

                    $sql = "SELECT * FROM pakket_afhaalpunt pa JOIN pakket p ON p.pakketID = pa.pakketID  WHERE afhaalpuntID = :afhaalpunt AND geaccepteerd <> 3"; //select de pakketjes
                    $db->query($sql);
                    $db->bind(":afhaalpunt", $afhaalpunt);
                    $db->execute();
                    $pakket = $db->resultSet();

                    date_default_timezone_set('Europe/Amsterdam');
                    $h = date("h" - 12) ;
                    $i = date("i");
                    $s = date("s");
                    $datumTijd = date("Y-m-d $h:$i:$s");

                    foreach($pakket as $value) {
                        print("PakketID: ".$value['pakketID'] . "<br>");
                        print("opname tijd: ". $value['opname'] . "<br>");
                        print("huidige tijd: " . date("Y-m-d H:i:s"));

                        if($value['opname'] < $datumTijd) {
                            print("<br> pakketje is grijs <br><br>");
                            $groen = false;
                        }
                        else {
                            print("<br>pakketje is groen <br> <br>");
                            $groen = true;
                        }
                    }
                    ?>
                        <form method="post" action="">
                            <input type="submit" name="grijsWeg" value="Alle grijze pakketjes weg">
                            <input type="submit" name="haalBezorger" value="Vraag bezorger om pakketjes op te halen">
                        </form>
                    <?php
                    }

                    else if (isset($_POST['uitgeven'])) {
                        $query = "SELECT * FROM pakket_werknemer WHERE opnameAfhaalpuntID = :afhaalpunt AND afgifte LIKE '%0000%' AND opname LIKE '%0000%'";
                        $db->query($query);
                        $db->bind(':afhaalpunt', $afhaalpunt);
                        $db->execute();
                        $result = $db->resultSet();


                        if (!empty($result)) {
                            ?>

                            <h3 style="color: green; text-align: center;">Dit zijn alle pakketten die
                                uitgegeven kunnen worden <br>
                                Als de werknemer er niet tussen staat, dan moet hij zich opnieuw aanmelden voor het
                                pakket <br> Als je op het pakketje drukt, kan je gelijk het pakketje overhandigen.
                                <br>
                                CONTROLEER GOED OP ZIJN WERKNEMER ID!

                            </h3>
                            <?php

                            foreach ($result as $value) {
                                $query = "SELECT * FROM pakket WHERE pakketID = :pakketID";
                                $db->query($query);
                                $db->bind(':pakketID', $value['pakketID']);
                                $db->execute();
                                $resultpakket = $db->single();

                                ?>


                                <div class="text-center">
                                    <form method="post" action="kioskAanmelding.php" id="pakketten">

                                        <button type="submit" name="pakketten2"
                                                value="<?php print($value['pakketID']); ?>">
                                            <br>
                                            <?php

                                            print("PakketID = " . $resultpakket['pakketID'] . "<br>");
                                            print("werknemerID = " . $value['werknemerID'] . "<br>");
                                            print("afhaalpunt = " . $afhaalStation . "<br>");
                                            print("Lengte = " . $resultpakket['pakketLengte'] . "CM<br>");
                                            print("Breedte = " . $resultpakket['pakketBreedte'] . "CM<br>");
                                            print("Hoogte = " . $resultpakket['pakketHoogte'] . "CM<br>");
                                            print("Gewicht = " . $resultpakket['pakketGewicht'] . "gram<br>");

                                            ?>
                                        </button>
                                    </form>
                                </div>
                                <?php
                            }

                        } else {
                            print("<h3 style=\"color: red; text-align: center;\">HELAAS! geen pakketjes gevonden <br> Mocht de bezorger
                                            er zeker van zijn dat hij zich heeft aangemeld, dan moet hij het gewoon nog een proberen. <br> Dan heeft hij niet binnen 30min het pakketje opgehaald.
                                                </h3>");
                        }


                    }


                }
            }

            ?>


        </div>
    </div>
</div>


<?php
require_once('/includes/footer.php');

?>