<?php
require_once("/includes/header.php");

?>
<?php
if ($_SESSION['user']['userlevel'] != 5) {
    ?>
    <script> window.location.replace("index.php"); </script> <?php
}
?>

<div class="container">
    <div class="row">
        <!--        $_SESSION['user']['']-->
        <div class="col-md-12 col-xs-8 border">

            <?php

            date_default_timezone_set('Europe/Amsterdam');
            $test = date("Y-m-d H:i:s");

            //haalt het opgegeven pakket die bij pakektOphalen is opgegeven
            $query = 'SELECT * FROM pakket WHERE pakketID = :pakketID';
            $db->query($query);
            $db->bind(':pakketID', $_POST['pakketID']);
            $db->execute();
            $result2 = $db->single();


            // in de DB word gezet dat deze gebruiker zich heeft aangemeld voor dit pakketje. Als hij hem niet binnen 30min ophaald word hij weer verwijderd.
            $query = 'INSERT INTO pakket_werknemer
                      (pakketID, werknemerID, opname, afgifte, opnameAfhaalpuntID, afgifteAfhaalpuntID, aanvraagTijd )
                      VALUES (:pakketID, :werknemerID, :opname, :afgifte, :opnameAfhaalpuntID, :afgifteAfhaalpuntID, :aanvraagTijd)
                                  ';
            $db->query($query);
            $db->bind(':pakketID', $result2['pakketID']);
            $db->bind(':werknemerID', $_SESSION['user']['wID']);
            $db->bind(':opname', "0000-00-00 00:00:00");
            $db->bind(':afgifte', '0000-00-00 00:00:00');
            $db->bind(':opnameAfhaalpuntID', $result2['pakketBeginpunt']);
            $db->bind(':afgifteAfhaalpuntID', $result2['pakketEindpunt']);
            $db->bind(':aanvraagTijd', $test);
            $db->execute();


            ?>

            <h3 style="color: green; text-align: center;">Het is helemaal gelukt! je kan nu het pakketje ophalen bij de
                kiosk! <br> Vergeet je niet je te legitimeren voordat je je pakketje ophaald?<br>
                Wij bedanken je voor het gebruiken van onze service! <br>Samen met jou zorgen we voor een beter millieu!<br>TOPPER!
            </h3>


            <br>
            <br>
        </div>
    </div>
</div>


<?php
require_once('/includes/footer.php');

/**
 * Created by PhpStorm.
 * User: stefa
 * Date: 4/27/2017
 * Time: 8:47 PM
 */
?>
