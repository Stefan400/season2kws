<?php
require_once("/includes/header.php");
$error1 = false;
$checkboxes = array();
if ($_SESSION['user']['userlevel'] != 5) {
    ?>
    <script> window.location.replace("index.php"); </script> <?php
    exit();
}
?>

<div class="container">

    <div class="row">

        <?php


        //dit word alleen gedaan als iemand al een pakketje had, maar zijn gedachtte veranderde en op cancel heeft gedrukt
        if (isset($_POST['cancel'])) {
            $query = "DELETE FROM pakket_werknemer WHERE werknemerID = :werknemerID AND opname LIKE '%0000%' AND afgifte LIKE '%0000%'";
            $db->query($query);
            $db->bind(':werknemerID', $_SESSION['user']['wID']);
            $db->execute();
        } else {


            //als er al een pakketje is die is aanngekomen, maar nog niet is opgehaald, dan word dit laten zien
            $query = "SELECT werknemerID FROM pakket_werknemer WHERE werknemerID = :werknemerID AND opname LIKE '%0000%' AND afgifte LIKE '%0000%'";
            $db->query($query);
            $db->bind(':werknemerID', $_SESSION['user']['wID']);
            $db->execute();
            $resul = $db->single();

            if ($resul > 0) {
                ?>
                <h3 style="color: red; text-align: center;">Volgens onze gegevens heb je al een pakketje. <br> Als je
                    die wilt cancelen, druk dan op de knop hieronder!
                </h3>

                <form method="post" action="pakketOphalen.php" id="pakketten">
                    <button type="submit" name="cancel" class="col-md-3 col-xs-10">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        Pakketje cancelen!
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    </button>
                </form>
                <?php

            } else {


                date_default_timezone_set('Europe/Amsterdam');
                $test = date("Y-m-d H:i:s", strtotime("-30 minutes"));

                //wanneer er een tabje bij de DB in komt om te kijken of iemand een pakketje ook daadwerkelijk heeft aangenomen, kan je hiermee verder
                $query = "SELECT aanvraagTijd, opname, pakketID FROM pakket_werknemer WHERE opname LIKE '%0000%' OR opname = NULL";
                $db->query($query);
                $db->execute();
                $result = $db->resultSet();

                // alle pakketten die langer dan 30 minuten in de kiosk liggen, en die niet zijn opgehaald, word hiermee weer vrij gegeven
                for ($counter = 0; ($counter < count($result)); $counter++) {
                    if ($result[$counter]['aanvraagTijd'] < $test) {
                        $query = "DELETE FROM pakket_werknemer WHERE pakketID = :pakketid";
                        $db->query($query);
                        $db->bind(':pakketid', $result[$counter]['pakketID']);
                        $db->execute();
                        //deze moet eruit
                    }
                }


                // haalt alle afhaalpunten uit de DB
                $query = 'SELECT afhaalpuntID, afhaalpuntStation FROM afhaalpunt';
                $db->query($query);
                $db->execute();
                $result = $db->resultSet();
                if (!isset($_POST['verzenden']) || empty($_POST['radio']) || (empty($_POST['1']) && empty($_POST['2']) && empty($_POST['3']) && empty($_POST['4']) && empty($_POST['5']) && empty($_POST['6']) && empty($_POST['7']))) {
                    ?>
                    <div class="col-md-2 col-xs-2border">
                        <!-- placeholder text
                        Wat fijn dat je weer een pakketje wilt mee nemen. Selecteer hier het station waarvan je vertrekt. Dan zorgen wij dat je een pakketje krijgt toegewezen!
                        -->
                    </div>

                    <div class="col-md-8 col-xs-8 border">

                        <h1 align="center"> Pakket ophalen </h1>
                        <br>
                        <br>

                        <form method="post" action="pakketOphalen.php" id="pakketten">

                            <div class="col-md-5 col-xs-6 border thumbnail">
                                <h4>Selecteer jou begin station!</h4>

                                <?php


                                // zorgt voor radio buttons voor zijn/haar begin station
                                foreach ($result as $item) {
                                    ?>
                                    <ul>
                                        <td><label><input type="radio" name=radio
                                                          value="<?php print($item['afhaalpuntStation']) ?>"><?php print($item['afhaalpuntStation']) ?>
                                            </label></td>
                                    </ul>
                                    <?php
                                }
                                ?>

                                <br>
                            </div>
                            <div class="col-md-1 col-xs-6 border">
                            </div>
                            <div class="col-md-5 col-xs-6 border thumbnail">
                                <h4>Bij welke stations wil je iets bezorgen?</h4>
                                <?php

                                // laat alle stations zien waar de bezorger een pakketje wilt bezorgen
                                foreach ($result as $item) {
                                    ?>

                                    <ul>
                                        <td><label><input type="checkbox" name="<?php print($item['afhaalpuntID']) ?>"
                                                          value="<?php print($item['afhaalpuntStation']) ?>"><?php print($item['afhaalpuntStation']) ?>
                                            </label></td>
                                    </ul>
                                    <?php
                                }
                                ?>


                            </div>


                            <div class="col-md-9 col-xs-2border">
                            </div>

                            <input class="btn btn-primary" align="center" type="submit" name="verzenden"
                                   value="verzenden">
                            <br>
                            <br>

                        </form>


                    </div>


                    <div class="col-md-2 col-xs-2 col-centered">

                    </div>
                    <?php

                    // zet alle checkboxes in een array
                } else if (isset($_POST['verzenden'])) {
                    for ($counter = 1; ($counter - 1 < count($result)); $counter++) {
                        if (isset($_POST[$counter])) {
                            array_push($checkboxes, $_POST[$counter]);
                        }
                    }


                    ?>
                    <!-- placeholder tekst
                    <div class="col-md-2 col-xs-2border">
                        Heujj we zijn bij stap twee!!! LEKKER BEZIG MAN! OF VROUW! of apachihelikoper... w/e. We zijn er bijna! na deze pagina kan je je pakketje ophalen bij je opgegeven kiosk :) GA ZO DOOR TOPPER!!!!
                        Controleer ff dit pakketje jongeh! Als het je aanstaat gewoon aannemen die handel yo.
                    </div>
                    -->
                    <div class="col-md-12 col-xs-8 border text-center">
                        <h1 align="center"> Selecteer het pakket dat jij wilt bezorgen</h1>

                        <br>
                        <br>

                        <form method="post" action="pakketOphalen2.php" id="pakketten">

                            <!--                    ELECT * FROM pakket WHERE geaccepteerd = 1 AND pakketEindpunt =-->
                            <!--                    (SELECT afhaalpuntID FROM afhaalpunt WHERE afhaalpuntStation LIKE '%amsterdam%' OR afhaalpuntStation = 'amsterdam') AND-->
                            <!--                    pakketBeginpunt = (SELECT afhaalpuntID FROM afhaalpunt WHERE afhaalpuntStation LIKE '%rotterdam%') AND pakketID NOT IN-->
                            <!--                    (SELECT pakketID FROM pakket_werknemer WHERE opname LIKE '%0000%' OR afgifte LIKE '%0000%')-->
                            <!--                    ORDER BY pakketVerzenddatum ASC LIMIT 1-->


                            <!--                    oude-->
                            <!--                    $query = "SELECT * FROM pakket p JOIN pakket_afhaalpunt a ON p.pakketID = a.pakketID WHERE p.pakketEindpunt =-->
                            <!--                    (SELECT afhaalpuntID FROM afhaalpunt WHERE afhaalpuntStation LIKE :afhaalpunt OR afhaalpuntStation = :afhaalpunt2) AND-->
                            <!--                    p.pakketBeginpunt = (SELECT afhaalpuntID FROM afhaalpunt WHERE afhaalpuntStation LIKE :beginpunt) AND-->
                            <!--                    p.pakketID NOT IN (SELECT pakketID FROM pakket_werknemer WHERE opname LIKE '%0000%')-->
                            <!--                    AND p.geaccepteerd = '1'-->
                            <!--                    ORDER BY p.pakketVerzenddatum ASC limit 1";-->

                            <?php
                            $u = 0;

                            // voor elk punt dat de bezorger heeft aangegeven waar hij wilt bezorgen, word een pakketje laten zien die het langst in de kiosk heeft gelegen.
                            foreach ($checkboxes as $value) {
                                $query = "SELECT * 
                                        FROM pakket
                                        WHERE geaccepteerd =1
                                        AND pakketEindpunt = (
                                        
                                        SELECT afhaalpuntID
                                        FROM afhaalpunt
                                        WHERE afhaalpuntStation LIKE  :afhaalpunt
                                        OR afhaalpuntStation =  :afhaalpunt2
                                        )
                                        AND pakketBeginpunt = ( 
                                        SELECT afhaalpuntID
                                        FROM afhaalpunt
                                        WHERE afhaalpuntStation LIKE  :beginpunt ) 
                                        AND pakketID NOT IN (
                                        SELECT pakketID
                                        FROM pakket_werknemer
                                        WHERE opname LIKE  '%0000%'
                                        OR afgifte LIKE  '%0000%'
                                        )
                                        ORDER BY pakketVerzenddatum ASC 
                                        LIMIT 1";
                                $db->query($query);
                                $db->bind(':afhaalpunt', '%' . $value . '%');
                                $db->bind(':afhaalpunt2', $value);
                                $db->bind(':beginpunt', '%' . $_POST['radio'] . '%');
                                $db->execute();
                                $result = $db->single();


                                if ($result > 0) {
                                    $u++;

                                    ?>

                                    <!--                                laat alle buttons zien. als je op 1 klinkt word het pakketID mee gegeven -->
                                    <button type="submit" name="pakketID" value="<?php print($result['pakketID']); ?>">
                                        <br>
                                        <?php
                                        print("Lengte = " . $result['pakketLengte'] . "CM<br>");
                                        print("Breedte = " . $result['pakketBreedte'] . "CM<br>");
                                        print("Hoogte = " . $result['pakketHoogte'] . "CM<br>");
                                        print("Gewicht = " . $result['pakketGewicht'] . "Kilo<br>");
                                        print("Van " . $_POST['radio'] . "<br>");
                                        print("Naar " . $value . "<br>");

                                        ?>

                                    </button>

                                    <?php
                                }


                            }
                            if ($u == 0) {
                                ?>
                                <h1 style="color: red; text-align: center;"> HELAAS geen pakketten beschikbaar! </h1>
                                <?php
                            }
                            ?>
                            <br>
                            <br>
                            <br>


                            <div class="col-md-9 col-xs-2border">
                            </div>

                            <br>
                            <br>

                        </form>


                    </div>


                    <div class="col-md-2 col-xs-2 col-centered">

                    </div>
                    <?php
                }
            }
            ?>


        <?php } ?>
    </div>
</div>


<?php
require_once('/includes/footer.php');
?>
