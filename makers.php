<!DOCTYPE HTML>
<HTML>
<?php
require_once('/includes/header.php');
require_once ("/includes/headerLinks.php")
?>


    <link rel="stylesheet" href="/season2/styling/css/makers.css">
    <link rel="stylesheet" href="/season2/styling/js/makers.js">


<div class="container" style="background: white;">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="col-md-4 col-sm-6">
                <div class="card-container">
                    <div class="card">
                        <div class="front">
                            <div class="cover">
                                <img src="https://www.clipartsgram.com/image/129556292-kyz84k3.jpg"/>
                            </div>
                            <div class="user">
                                <img class="img-circle"
                                     src="https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAATDAAAAJGRlNjNiZjA2LTdiMmQtNDdkYi05YTBkLTEzNzA4ZjljZTc2MA.jpg"/>
                            </div>
                            <div class="content">
                                <div class="main">
                                    <h3 class="name">Stefan Schooten</h3>
                                    <p class="profession">SCRUM MASTER</p>
                                    <p class="text-center">"Ik verzorg de daily standup's, globale planning en ik heb
                                        veel geprogrammeerd in JAVA en PHP."</p>
                                </div>
                                <div class="footer">
                                    <i class="fa fa-mail-forward"></i> Auto Rotation
                                </div>
                            </div>
                        </div> <!-- end front panel -->
                        <div class="back">
                            <div class="header">
                                <h5 class="motto">"To be or not to be, this is my awesome motto!"</h5>
                            </div>
                            <div class="content">
                                <div class="main">
                                    <h4 class="text-center">Job Description</h4>
                                    <p class="text-center">Ik verzorg de daily standup's, globale planning en ik heb
                                        veel geprogrammeerd in JAVA en PHP.</p>

                                    <div class="stats-container">
                                        <div class="stats">
                                            <h4>235</h4>
                                            <p>
                                                Followers
                                            </p>
                                        </div>
                                        <div class="stats">
                                            <h4>114</h4>
                                            <p>
                                                Following
                                            </p>
                                        </div>
                                        <div class="stats">
                                            <h4>35</h4>
                                            <p>
                                                Projects
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="footer">
                                <div class="social-links text-center">
                                    <a href="http://deepak646.blogspot.in/" class="facebook"><i
                                                class="fa fa-facebook fa-fw"></i></a>
                                    <a href="http://deepak646.blogspot.in/" class="google"><i
                                                class="fa fa-google-plus fa-fw"></i></a>
                                    <a href="http://deepak646.blogspot.in/" class="twitter"><i
                                                class="fa fa-twitter fa-fw"></i></a>
                                </div>
                            </div>
                        </div> <!-- end back panel -->
                    </div> <!-- end card -->
                </div> <!-- end card-container -->
            </div> <!-- end col sm 3 -->
            <!--         <div class="col-sm-1"></div> -->
            <div class="col-md-4 col-sm-6">
                <div class="card-container">
                    <div class="card">
                        <div class="front">
                            <div class="cover">
                                <img src="https://www.clipartsgram.com/image/129556292-kyz84k3.jpg"/>
                            </div>
                            <div class="user">
                                <img class="img-circle"
                                     src="https://i.gyazo.com/e223bf134b3d7f475dc7a1172ad26025.png"/>
                            </div>
                            <div class="content">
                                <div class="main">
                                    <h3 class="name">Mathijs Breuker</h3>
                                    <p class="profession">Web-developer</p>
                                    <p class="text-center">"I'm the new Ron Jeremy, and since I made it here I can make it
                                        anywhere, yeah, they love me everywhere"</p>
                                </div>
                                <div class="footer">
                                    <i class="fa fa-mail-forward"></i> Auto Rotation
                                </div>
                            </div>
                        </div> <!-- end front panel -->
                        <div class="back">
                            <div class="header">
                                <h5 class="motto">"To do or not to do, this is my awesome Ron Jeremotto!"</h5>
                            </div>
                            <div class="content">
                                <div class="main">
                                    <h4 class="text-center">Job Description</h4>
                                    <p class="text-center">Web design, Adobe Photoshop, HTML5, CSS3, Corel and many
                                        others...</p>

                                    <div class="stats-container">
                                        <div class="stats">
                                            <h4>235</h4>
                                            <p>
                                                Followers
                                            </p>
                                        </div>
                                        <div class="stats">
                                            <h4>114</h4>
                                            <p>
                                                Following
                                            </p>
                                        </div>
                                        <div class="stats">
                                            <h4>35</h4>
                                            <p>
                                                Projects
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="footer">
                                <div class="social-links text-center">
                                    <a href="http://deepak646.blogspot.in/" class="facebook"><i
                                                class="fa fa-facebook fa-fw"></i></a>
                                    <a href="http://deepak646.blogspot.in/" class="google"><i
                                                class="fa fa-google-plus fa-fw"></i></a>
                                    <a href="http://deepak646.blogspot.in/" class="twitter"><i
                                                class="fa fa-twitter fa-fw"></i></a>
                                </div>
                            </div>
                        </div> <!-- end back panel -->
                    </div> <!-- end card -->
                </div> <!-- end card-container -->
            </div> <!-- end col sm 3 -->
            <!--         <div class="col-sm-1"></div> -->
            <div class="col-md-4 col-sm-6">
                <div class="card-container">
                    <div class="card">
                        <div class="front">
                            <div class="cover">
                                <img src="https://www.clipartsgram.com/image/129556292-kyz84k3.jpg"/>
                            </div>
                            <div class="user">
                                <img class="img-circle"
                                     src="https://i.gyazo.com/3344615816bbea2adaabb951416d3c44.png"/>
                            </div>
                            <div class="content">
                                <div class="main">
                                    <h3 class="name">Erik van Elten</h3>
                                    <p class="profession">DOC-ADMINISTATOR</p>
                                    <p class="text-center">"DOCTER"</p>
                                </div>
                                <div class="footer">
                                    <i class="fa fa-mail-forward"></i> Auto Rotation
                                </div>
                            </div>
                        </div> <!-- end front panel -->
                        <div class="back">
                            <div class="header">
                                <h5 class="motto">"To be or not to be, this is my awesome motto!"</h5>
                            </div>
                            <div class="content">
                                <div class="main">
                                    <h4 class="text-center">Job Description</h4>
                                    <p class="text-center">Web design, Adobe Photoshop, HTML5, CSS3, Corel and many
                                        others...</p>

                                    <div class="stats-container">
                                        <div class="stats">
                                            <h4>235</h4>
                                            <p>
                                                Followers
                                            </p>
                                        </div>
                                        <div class="stats">
                                            <h4>114</h4>
                                            <p>
                                                Following
                                            </p>
                                        </div>
                                        <div class="stats">
                                            <h4>35</h4>
                                            <p>
                                                Projects
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="footer">
                                <div class="social-links text-center">
                                    <a href="http://deepak646.blogspot.in/" class="facebook"><i
                                                class="fa fa-facebook fa-fw"></i></a>
                                    <a href="http://deepak646.blogspot.in/" class="google"><i
                                                class="fa fa-google-plus fa-fw"></i></a>
                                    <a href="http://deepak646.blogspot.in/" class="twitter"><i
                                                class="fa fa-twitter fa-fw"></i></a>
                                </div>
                            </div>
                        </div> <!-- end back panel -->
                    </div> <!-- end card -->
                </div> <!-- end card-container -->
            </div> <!-- end col sm 3 -->

            <!--         <div class="col-sm-1"></div> -->
            <div class="col-md-4 col-sm-6">
                <div class="card-container">
                    <div class="card">
                        <div class="front">
                            <div class="cover">
                                <img src="https://www.clipartsgram.com/image/129556292-kyz84k3.jpg"/>
                            </div>
                            <div class="user">
                                <img class="img-circle"
                                     src="https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAAceAAAAJDdiYmY0YzhiLTY4MjItNGNlOS1iMDlhLTIxY2VjMGMwNWMyOQ.jpg"/>
                            </div>
                            <div class="content">
                                <div class="main">
                                    <h3 class="name">Nick Hoogkamp</h3>
                                    <p class="profession">Technical Manager</p>
                                    <p class="text-center">"Motivatie is de sleutel tot een team. Ik zorg er dan ook voor dat iedereen geinspireerd wordt door ons product!"</p>
                                </div>
                                <div class="footer">
                                    <i class="fa fa-mail-forward"></i> Auto Rotation
                                </div>
                            </div>
                        </div> <!-- end front panel -->
                        <div class="back">
                            <div class="header">
                                <h5 class="motto">"Het leven is een spel, en wij als spelers mogen er met veel plezier aan meedoen!"</h5>
                            </div>
                            <div class="content">
                                <div class="main">
                                    <h4 class="text-center">Job Description</h4>
                                    <p class="text-center">Development, framework engineering, Developmentcoordinatie.</p>

                                    <div class="stats-container">
                                        <div class="stats">
                                            <h4>235</h4>
                                            <p>
                                                Followers
                                            </p>
                                        </div>
                                        <div class="stats">
                                            <h4>114</h4>
                                            <p>
                                                Following
                                            </p>
                                        </div>
                                        <div class="stats">
                                            <h4>35</h4>
                                            <p>
                                                Projects
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="footer">
                                <div class="social-links text-center">
                                    <a href="http://deepak646.blogspot.in/" class="facebook"><i
                                                class="fa fa-facebook fa-fw"></i></a>
                                    <a href="http://deepak646.blogspot.in/" class="google"><i
                                                class="fa fa-google-plus fa-fw"></i></a>
                                    <a href="http://deepak646.blogspot.in/" class="twitter"><i
                                                class="fa fa-twitter fa-fw"></i></a>
                                </div>
                            </div>
                        </div> <!-- end back panel -->
                    </div> <!-- end card -->
                </div> <!-- end card-container -->
            </div> <!-- end col sm 3 -->
            <div class="col-md-4 col-sm-6">
                <div class="card-container">
                    <div class="card">
                        <div class="front">
                            <div class="cover">
                                <img src="https://www.clipartsgram.com/image/129556292-kyz84k3.jpg"/>
                            </div>
                            <div class="user">
                                <img class="img-circle"
                                     src="https://i.gyazo.com/b7d660c581712b049bb9b63a6613bc44.png"/>
                            </div>
                            <div class="content">
                                <div class="main">
                                    <h3 class="name">Gerjan Hertgers</h3>
                                    <p class="profession">JUNIOR PROGRAMMER</p>

                                    <p class="text-center">"Ik verzorg de opmaak en functionaliteit van deze website door middel van HTML, CSS, Bootstrap, JavaScript en PHP"</p>
                                </div>
                                <div class="footer">
                                    <div class="rating">
                                        <i class="fa fa-mail-forward"></i> Auto Rotation
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end front panel -->
                        <div class="back">
                            <div class="header">
                                <h5 class="motto">"No Motto"</h5>
                            </div>
                            <div class="content">
                                <div class="main">
                                    <h4 class="text-center">Job Description</h4>
                                    <p class="text-center">HTML, CSS, PHP en een klein beetje JAVA</p>

                                    <div class="stats-container">
                                        <div class="stats">
                                            <h4>235</h4>
                                            <p>
                                                Followers
                                            </p>
                                        </div>
                                        <div class="stats">
                                            <h4>114</h4>
                                            <p>
                                                Following
                                            </p>
                                        </div>
                                        <div class="stats">
                                            <h4>35</h4>
                                            <p>
                                                Projects
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="footer">
                                <div class="social-links text-center">
                                    <a href="http://deepak646.blogspot.in/" class="facebook"><i
                                                class="fa fa-facebook fa-fw"></i></a>
                                    <a href="http://deepak646.blogspot.in/" class="google"><i
                                                class="fa fa-google-plus fa-fw"></i></a>
                                    <a href="http://deepak646.blogspot.in/" class="twitter"><i
                                                class="fa fa-twitter fa-fw"></i></a>
                                </div>
                            </div>
                        </div> <!-- end back panel -->
                    </div> <!-- end card -->
                </div> <!-- end card-container -->
            </div> <!-- end col-sm-3 -->
            <div class="col-md-4 col-sm-6">
            </div> <!-- end col sm 3 -->
        </div> <!-- end col-sm-10 -->
    </div> <!-- end row -->

</div>

<?php
require_once('/includes/footer.php');
?>



</html>
