
<?php
require_once("/includes/header.php");
?>
<div class="container">

    <div class="row">


        <div class="col-md-8 col-xs-8 border">
            <h1>OnTrack for a better environment!</h1>
            <h2>Groen zien</h2>
            <p>
                OnTrack wil het beste voor jou en voor het milieu! Daarom focussen
                wij op het vervangen van het grijze transport door een ecovriendelijke,
                groene variant! Weg met al die vrachtwagens en bestelbusjes! Weg
                met al die overbodige CO2-uitstoot. Wij maken het waar, samen met jou!
            </p>
            <h2>Gaan als een trein</h2>
            <p>
                OnTrack heeft een collectie aan toegewijde bezorgers die dagelijks
                gebruik maken van het openbaar vervoer! Deze bezorgers brengen jouw
                pakketje van station A naar station B. Minder CO2-uitstoot door 
                meer gebruik te maken van de faciliteiten! Zo ziet OnTrack het 
                graag gebeuren!
            </p>
            <h2>Ding dong!</h2>
            <p>
                Heb je iets beters te doen dan naar het station lopen voor je pakketje?
                Wij bieden je de optie om een pakket zowel van als aan huis te
                bezorgen! OnTrack heeft de armen namelijk in elkaar gehaakt met
                je lokale fietskoerier, zodat jij niet meer hoeft te doen dan nodig is!
                Laat ons ons maar zorgen maken over jouw pakket, zodat jij je kunt
                focussen op wat voor jou belangrijk is!
            </p>

            <br>
        </div>
        
        <div class="col-md-4 col-xs-4 col-centered thumbnail">
            <image src='https://pbs.twimg.com/media/C-L4ZZIU0AAaTSo.jpg' style='width: 100%; height: 60%;'/>
        </div>
    </div>
</div>

<?php
require_once('/includes/footer.php');

/**
 * Created by PhpStorm.
 * User: stefa
 * Date: 4/27/2017
 * Time: 8:47 PM
 */
?>