<!DOCTYPE HTML>

<?php
require_once("/includes/header.php");
require_once("/includes/headerLinks.php");

//lege variabelen
$error1 = "";
$required = "";
$aanhef = "";
$action = "";
$result = "";
$afzenderID = 2;

//fucties
function required($input)
{
    global $error1;
    if (isset($_POST['next']) && empty($_POST[$input])) {
        $error1 = true;
        return ' style="color:red;"';

    }
}

// als er op de NEXT knop is gedrukt
if (isset($_POST["next"])) {
if (!empty($_POST["aanhef"]) && !empty($_POST["voornaam"]) && !empty($_POST["achternaam"]) && !empty($_POST["adres"]) && !empty($_POST["postcode"]) && !empty($_POST["woonplaats"])) {
$voornaam = $_POST['voornaam'];
$achternaam = $_POST['achternaam'];
$adres = $_POST['adres'];
$postcode = $_POST['postcode'];
$woonplaats = $_POST['woonplaats'];
// Check of persoon is ingelogd

//Check of ingevulde gegevens al bestaan
$sql = "SELECT ontvangerVoornaam, ontvangerAchternaam, ontvangerAdres, ontvangerPostcode, ontvangerWoonplaats 
                FROM ontvanger WHERE ontvangerVoornaam='" . $voornaam . "' AND ontvangerAchternaam='" . $achternaam . "' AND ontvangerAdres='" . $adres . "' AND ontvangerPostcode='" . $postcode . "' AND ontvangerWoonplaats='" . $woonplaats . "'";
$db->query($sql);
$db->execute();
$result = $db->Single();

if ($result == 0) {

// INSERT query
$query = 'INSERT INTO ontvanger
             (ontvangerAanhef, ontvangerVoornaam, ontvangerTussenvoegsel, ontvangerAchternaam, ontvangerAdres, ontvangerPostcode, ontvangerWoonplaats, afzenderID)
              VALUES (:aanhef, :voornaam, :tussenvoegsel, :achternaam, :adres, :postcode, :woonplaats, :afzenderID)';

$db->query($query);
$db->bind(':aanhef', $_POST['aanhef']);
$db->bind(':voornaam', $_POST['voornaam']);
$db->bind(':tussenvoegsel', $_POST['tussenvoegsel']);
$db->bind(':achternaam', $_POST['achternaam']);
$db->bind(':adres', $_POST['adres']);
$db->bind(':postcode', $_POST['postcode']);
$db->bind(':woonplaats', $_POST['woonplaats']);
$db->bind(':afzenderID', $afzenderID);
$db->execute();

//Select de ontvangerID van de nieuwe aangemaakte ontvanger
$sql = "SELECT ontvangerID FROM ontvanger WHERE ontvangerVoornaam='" . $voornaam . "' AND ontvangerAchternaam='" . $achternaam . "' AND ontvangerAdres='" . $adres . "' AND ontvangerPostcode='" . $postcode . "' AND ontvangerWoonplaats='" . $woonplaats . "'";
$db->query($sql);
$db->execute();
$result = $db->Single();
$ontvangerID = $result['ontvangerID'];

?>

<script> window.location.replace("pakketAanmelden2.php?afzID=<?php print($afzenderID);?>&oID=<?php print($ontvangerID);?>"); </script> <!-- LET OP, STATISCHE Afzender ID-->
<?php
}
else {
    $sql = "SELECT ontvangerID FROM ontvanger WHERE ontvangerVoornaam='" . $voornaam . "' AND ontvangerAchternaam='" . $achternaam . "' AND ontvangerAdres='" . $adres . "' AND ontvangerPostcode='" . $postcode . "' AND ontvangerWoonplaats='" . $woonplaats . "'";
    $db->query($sql);
    $db->execute();
    $result = $db->Single();
    $ontvangerID = $result['ontvangerID'];

    ?>
    <script> window.location.replace("pakketAanmelden2.php?afzID=<?php print($afzenderID);?>&oID=<?php print($ontvangerID);?>"); </script> <!-- LET OP, STATISCHE Afzender ID--> <?php
}
}
}

?>

<link rel="stylesheet" href="/season2/styling/css/pakket.css">


<!-- Body container -->

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <div id="naglowek">
                <form action="<?php print($action); ?>" method="post">
                    <div class="container">
                        <div class="col-md-3 col-xs-2 border">
                        </div>
                        <div class="col-md-6 col-xs-12 border">
                            <h2> Ontvanger gegevens </h2>
                            <br>
                            <br>

                            <table>
                                <tr>
                                    <td <?php print(required("aanhef")) ?>> Aanhef</td>
                                    <td><input type="radio" name="aanhef"
                                               value='Dhr.' <?php if (isset($_POST["next"]) && !empty($_POST["aanhef"])) {
                                            if ($_POST["aanhef"] == "Dhr.") {
                                                print("Checked");
                                            }
                                        } ?> > Dhr.
                                        <input type="radio" name="aanhef"
                                               value="Mevr." <?php if (isset($_POST["next"]) && !empty($_POST["aanhef"])) {
                                            if ($_POST["aanhef"] == "Mevr.") {
                                                print("Checked");
                                            }
                                        } ?>> Mevr.
                                        <input type='radio' name='aanhef'
                                               value='Dhr/Mevr.' <?php if (isset($_POST["next"]) && !empty($_POST["aanhef"])) {
                                            if ($_POST["aanhef"] == "Dhr/Mevr.") {
                                                print("Checked");
                                            }
                                        } ?>> Onbekend
                                    </td>
                                </tr>
                                <tr>
                                    <td <?php print(required("voornaam")) ?>> Voornaam ontvanger</td>
                                    <td><input type="text" name="voornaam" class="form-control"
                                               value="<?php if (isset($_POST["next"]) && !empty($_POST["voornaam"])) {
                                                   print($_POST["voornaam"]);
                                               } ?>"></td>
                                </tr>
                                <tr>
                                    <td> Tussenvoegsel ontvanger</td>
                                    <td><input type="text" name="tussenvoegsel" class="form-control"
                                               value="<?php if (isset($_POST["next"]) && !empty($_POST["tussenvoegsel"])) {
                                                   print($_POST["tussenvoegsel"]);
                                               } ?>"></td>
                                </tr>
                                <tr>
                                    <td <?php print(required("achternaam")) ?>> Achternaam ontvanger</td>
                                    <td><input type="text" name="achternaam" class="form-control"
                                               value="<?php if (isset($_POST["next"]) && !empty($_POST["achternaam"])) {
                                                   print($_POST["achternaam"]);
                                               } ?>"></td>
                                </tr>
                                <tr>
                                    <td <?php print(required("adres")) ?>> Adres ontvanger</td>
                                    <td><input type="text" name="adres" class="form-control"
                                               value="<?php if (isset($_POST["next"]) && !empty($_POST["adres"])) {
                                                   print($_POST["adres"]);
                                               } ?>"></td>
                                </tr>
                                <tr>
                                    <td <?php print(required("postcode")) ?>> Postcode ontvanger</td>
                                    <td><input type="text" name="postcode" class="form-control"
                                               value="<?php if (isset($_POST["next"]) && !empty($_POST["postcode"])) {
                                                   print($_POST["postcode"]);
                                               } ?>"></td>
                                </tr>
                                <tr>
                                    <td <?php print(required("woonplaats")) ?>> Woonplaats ontvanger</td>
                                    <td><input type="text" name="woonplaats" class="form-control"
                                               value="<?php if (isset($_POST["next"]) && !empty($_POST["woonplaats"])) {
                                                   print($_POST["woonplaats"]);
                                               } ?>"</td>
                                </tr>
                            </table>
                            <br>
                            <input type="submit" name="next" value="NEXT" class="btn btn-primary">
                            <?php
                            if ($error1 == true) {
                                print("Niet alle velden zijn ingevuld");
                            }
                            ?>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
require_once('/includes/footer.php');

/**
 * Created by PhpStorm.
 * User: stefa
 * Date: 4/27/2017
 * Time: 8:47 PM
 */
?>
