<?php
require_once("./includes/header.php");
if($_SESSION == NULL) {
        ?> <script> window.location.replace("index.php"); </script><?php
    }
$errorA = false;
include $_SERVER['DOCUMENT_ROOT'] . "/season2/classes/hash.class.php";
require_once("./includes/password.php");
?>
<div class="container">
    <div class="row">
        <div class="col-md-8">  
            <form method="post" action="ProfielAanpassen.php">
            <table class="table">
                <tr>
                    <td>Aanhef: </td>
                    <td><input align="left"type="radio" value="Dhr." name="radio" <?php if ($_SESSION['user']['ah'] === "Dhr.") { print("checked");}?>> Dhr.
                        <input align="right" type="radio" value="Mvr." name="radio" <?php if ($_SESSION['user']['ah'] === "Mvr.") { print("checked");}?>> Mvr.</td>
                </tr>
                <tr>
                    <td>Voornaam:</td>
                    <td><input placeholder="Voornaam" inputmode="text" type="text" class="form-control" name="voornaam" value="<?php print($_SESSION['user']['vn']); ?>"></td>
                </tr>
                <tr>
                    <td>Tussenvoegsel:</td>
                    <td><input placeholder="tussenvoegsel" inputmode="text" type="text" class="form-control" name="tussenvoegsel" value="<?php print($_SESSION['user']['tv']); ?>"></td>
                </tr>
                <tr>
                    <td>Achternaam:</td>
                    <td><input placeholder="Achternaam" type="text" class="form-control" name="achternaam" value="<?php print($_SESSION['user']['an']); ?>"></td>
                </tr>
                <tr>
                    <td>Telefoonnummer:</td>
                    <td><input placeholder="06-12345678"  type="number" class="form-control" name="telefoonnr" value="<?php print($_SESSION['user']['tn']); ?>"></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><input placeholder="E-Mail" type="email" class="form-control" name="email" value="<?php print($_SESSION['user']['em']); ?>"></td>
                </tr>
                <tr>
                    <td>Woonplaats:</td>
                    <td><input placeholder="Woonplaats" type="text" class="form-control" name="woonplaats" value="<?php print($_SESSION['user']['wp']);?>"></td>                
                </tr>
                <tr>
                    <td>Adres:</td>
                    <td><input placeholder="Adres" type="text" class="form-control" name="adres" value="<?php print($_SESSION['user']['ad']); ?>"></td>
                </tr>
                <tr>
                    <td>Postcode:</td>
                    <td><input placeholder="Postcode" type="text" class="form-control" name="postcode" value="<?php print($_SESSION['user']['pc']); ?>"></td>
                </tr>
                <tr>
                    <td>Wachtwoord:</td>
                    <td><input placeholder="Wachtwoord" type="password" class="form-control" name="wachtwoord"></td>
                </tr>
                <tr>
                    <td>Wachtwoord herhalen:</td>
                    <td><input placeholder="Wachtwoord" type="password" class="form-control" name="wachtwoordherhalen"></td>
                </tr>
                <?php if (isset($_SESSION['user']['bd'])) {?>
                <tr>
                    <?php
                print("<td>Bedrijfsnaam:</td>"); ?>
                <td><?php print('<input placeholder="Bedrijfsnaam" type="text" class="form-control" name="bedrijfsnaam" value="' . $_SESSION['user']['bd'] . '">');} ?></td>
                </tr>
                <tr>
                    <td><input type="checkbox" name="bezorger" value="Bezorger">Ik wil bezorger worden!<br></td>
                </tr>
                <tr>
                    <td><div class="g-recaptcha" data-sitekey="6LeEkiEUAAAAAET8JrZ_OBYL23QsP-rRiaB32LON"></div></td>
                </tr>
                <tr>
                    <td><a href="Profiel.php">Terug</a></td>
                    <td><input class="btn btn-primary" align="center" type="submit" name="opslaan" value="opslaan"></td>
                </tr>      
            </table>
            </form>
            <p>Wachtwoord moet minimaal uit 8 karakters bestaan en een speciaal teken bevatten</p>
                <?php
                if(isset($_POST["opslaan"])) {
                    if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
                        $secret = '6LeEkiEUAAAAADhoD1eiUUqaTKgF2GCgjCWJwebd';
                        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
                        $responseData = json_decode($verifyResponse);
                        if($responseData->success){
                            
                        
                    
                
                $DBA = new DB();
                
                if ($_SESSION['user']['userlevel'] == 1) {
                $queryProfiel = "UPDATE afzender SET afzenderAanhef=:Aanhef, afzenderVoornaam=:Voornaam, afzenderTussenvoegsel=:Tussenvoegsel, afzenderAchternaam=:Achternaam,
                    afzenderTelefoonnummer=:Telefoonnummer, afzenderEmail=:Email, afzenderWoonplaats=:Woonplaats, afzenderAdres=:Adres, afzenderPostcode=:Postcode, afzenderBedrijf=:Bedrijfsnaam WHERE afzenderID = :id";
                } else {
                $queryProfiel = "UPDATE sollicitant SET sollicitantAanhef=:Aanhef, sollicitantVoornaam=:Voornaam, sollicitantTussenvoegsel=:Tussenvoegsel, sollicitantAchternaam=:Achternaam,
                    sollicitantTelefoonnummer=:Telefoonnummer, sollicitantEmail=:Email, sollicitantWoonplaats=:Woonplaats, sollicitantAdres=:Adres, sollicitantPostcode=:Postcode WHERE sollicitantID = :id";
                }
                
                $DBA->query($queryProfiel);
                
                if ($_SESSION['user']['userlevel'] == 1 && !isset($WerknemerID)) {
                $DBA->bind(':id', $_SESSION['user']['ID']);
                } else {
                $DBA->bind(':id',  $_SESSION['user']['sID']);   
                }
                $DBA->bind(':Aanhef', $_POST['radio']);
                $DBA->bind(':Voornaam', $_POST['voornaam']);
                $DBA->bind(':Tussenvoegsel', $_POST['tussenvoegsel']); 
                $DBA->bind(':Achternaam', $_POST['achternaam']);
                $DBA->bind(':Telefoonnummer', $_POST['telefoonnr']);
                $DBA->bind(':Email', $_POST['email']);
                $DBA->bind(':Adres', $_POST['adres']); 
                $DBA->bind(':Postcode', $_POST['postcode']); 
                $DBA->bind(':Woonplaats', $_POST['woonplaats']);
                if(isset($_POST["bedrijfsnaam"])) {
                $DBA->bind(':Bedrijfsnaam', $_POST['bedrijfsnaam']);
                }
                
                $DBA->execute();
                    
                    $_SESSION['user']['ah'] = $_POST['radio'];
                    $_SESSION['user']['vn'] = $_POST['voornaam'];
                    $_SESSION['user']['tv'] = $_POST['tussenvoegsel'];
                    $_SESSION['user']['an'] = $_POST['achternaam'];
                    $_SESSION['user']['tn'] = $_POST['telefoonnr'];
                    $_SESSION['user']['em'] = $_POST['email'];
                    $_SESSION['user']['ad'] = $_POST['adres'];
                    $_SESSION['user']['pc'] = $_POST['postcode'];
                    $_SESSION['user']['wp'] = $_POST['woonplaats'];
                    if(isset($_POST["bedrijfsnaam"])) {
                    $_SESSION['user']['bd'] = $_POST['bedrijfsnaam'];
                    }
                    
                        if (isset($_POST["bezorger"])){
                            
                            $querySolli = "INSERT INTO sollicitant (sollicitantAanhef, sollicitantVoornaam, sollicitantTussenvoegsel, sollicitantAchternaam, sollicitantTelefoonnummer, sollicitantEmail, sollicitantAdres, sollicitantPostcode, sollicitantWoonplaats)
                                VALUES (:sah, :svn, :stv, :san, :stn, :sem, :sad, :spc, :swp)";
                            $DBA->query($querySolli);
                            $DBA->bind(':sah', $_SESSION['user']['ah']);
                            $DBA->bind(':svn', $_SESSION['user']['vn']);
                            $DBA->bind(':stv', $_SESSION['user']['tv']);
                            $DBA->bind(':san', $_SESSION['user']['an']);
                            $DBA->bind(':stn', $_SESSION['user']['tn']);
                            $DBA->bind(':sem', $_SESSION['user']['em']);
                            $DBA->bind(':sad', $_SESSION['user']['ad']);
                            $DBA->bind(':spc', $_SESSION['user']['pc']);
                            $DBA->bind(':swp', $_SESSION['user']['wp']);
                            $DBA->execute(); 
                            
                            $naam = ($_SESSION['user']['vn'] . " " . $_SESSION['user']['tv'] . " " . $_SESSION['user']['an'] = $_POST['achternaam']);
                            $to = "fogsfles@gmail.com";
                            $subject = "Ik wil bezorger worden!";
                            $txt = ("Iemand wilt zich aanmelden als bezorgen!\n\n" .
                                    "\n Naam: " . $naam .
                                    "\n Telefoon: " . $_SESSION['user']['tn'] .
                                    "\n Email: " . $_SESSION['user']['em'] .
                                    "\n Adres: " . $_SESSION['user']['adres'] .
                                    "\n Postcode: " . $_SESSION['user']['pc'] .
                                    "\n Woonplaats: " . $_SESSION['user']['wp']);
                            $headers = "From: fogsfles@gmail.com";
                            $mm = " en je verzoek om bezorger te worden wordt afgehandeld!";
                            
                            
                            
                            mail($to,$subject,$txt,$headers);                            
                            }
                            
                            //wachtwoord veranderen
                            if ((!empty($_POST['wachtwoord'])) && (!empty($_POST['wachtwoordherhalen'])))  {
                                $special = preg_match("#[\\~\\`\\!\\@\\#\\$\\%\\^\\&\\*\\(\\)\\_\\-\\+\\=\\{\\}\\[\\]\\|\\:\\;\\&lt;\\&gt;\\.\\?\\/\\\\\\\\]+#", $_POST['wachtwoord']);
                                // komt het niet overeen? alert!
                                if ($_POST['wachtwoord'] != $_POST['wachtwoordherhalen']) {
                                ?> <div class="alert alert-danger" role="alert">Wachtwoorden komen niet overeen</div> <?php
                                //Komt het wel overeen? Voer aanpassing door naar DB
                                $errorA = true;
                                } else if (!$special || strlen($_POST['wachtwoord']) < 7) {
                                    ?> <div class="alert alert-danger" role="alert">Wachtwoord voldoet niet aan de eisen!</div> <?php
                                    $errorA = true; } 
                                else {
                                 $hasher = new Hash(strlen($_POST['wachtwoord']) * 4);
                                 $salt = $hasher->MakeHash();
                                 $password = password_hash($_POST['wachtwoord'], PASSWORD_BCRYPT, ['salt' => $salt]); //hashed het wachtwoord. 
                                 
                                 
                                $queryWW = "UPDATE account SET accountWachtwoord = :nww, accountSalt = :ns WHERE accountGebruikersnaam = :geb";
                                $DBA->query($queryWW);
                                $DBA->bind(':nww', $password);
                                $DBA->bind(':ns', $salt);
                                $DBA->bind(':geb', $_SESSION['user']['login']);
                                $DBA->execute(); }
                            }

                    if ($errorA == false) {
                    echo("<script>alert('De resultaten zijn opgeslagen " . $mm . "');</script>");
                    echo("<script>window.onload = function(){location.href = 'Profiel.php'};</script>");
                    }
                    
                }
                } else {
                   ?> <div class="alert alert-success" role="alert">De Recaptcha is niet goed ingevuld</div> <?php
                }
                }
                
            
                
                
                
                
                
                
                
                
                
                
                
                
                ?>
            
            
        </div>    
    </div>
</div>












<?php
require_once("./includes/footer.php");


        ?>