<?php
require_once("/includes/header.php");
require_once("/includes/headerLinks.php");

//lege variabelen
$inhoud = "";
$afzenderID = $_GET["afzID"];
$ontvangerID = $_GET["oID"];

//fucties
function required($input)
{
    global $error1;
    if (isset($_POST['next']) && empty($_POST[$input])) {
        $error1 = true;
        return ' style="color:red;"';

    }
}

//happyflow
if (isset($_POST["next"]) && !empty($_POST["lengte"]) && !empty($_POST["breedte"]) && !empty($_POST["hoogte"]) && !empty($_POST["gewicht"])) {
    $inhoud = $_POST["lengte"] * $_POST["breedte"] * $_POST["hoogte"];
    $lengte = $_POST["lengte"];
    $breedte = $_POST["breedte"];
    $hoogte = $_POST["hoogte"];
    $gewicht = $_POST["gewicht"];
    $opmerking = $_POST["opmerking"];

    if ($inhoud <= 24848 && $gewicht <= 4000) {
        $groen = 1;
    } else {
        $groen = 0;
    }
    ?>
    <script> window.location.replace("pakketAanmelden3.php?afzID=<?php print($afzenderID);?>&oID=<?php print($ontvangerID);?>&l=<?php print($lengte);?>&b=<?php print($breedte);?>&h=<?php print($hoogte);?>&g=<?php print($gewicht);?>&gr=<?php print($groen);?>"); </script>
    <?php
}


?>


<link rel="stylesheet" href="/season2/styling/css/pakket.css">


<form method="post" id="form">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div id="naglowek">
                    <form action="<?php print($action); ?>" method="post">
                        <div class="container">
                            <div class="col-md-3 col-xs-2 border">
                            </div>
                            <div class="col-md-6 col-xs-12 border">
                                <h2> Pakket gegevens </h2>
                                <br>
                                <br>
                                <table>
                                    <tr>
                                        <td <?php print(required("lengte")) ?>> Lengte</td>
                                        <td><input type="number" name="lengte" placeholder="cm" class="form-control"
                                        </td>
                                    </tr>
                                    <tr>
                                        <td <?php print(required("breedte")) ?>> Breedte</td>
                                        <td><input type="number" name="breedte" placeholder="cm" class="form-control"
                                        </td>
                                    </tr>
                                    <tr>
                                        <td <?php print(required("hoogte")) ?>> Hoogte</td>
                                        <td><input type="number" name="hoogte" placeholder="cm" class="form-control"
                                        </td>
                                    </tr>
                                    <tr>
                                        <td <?php print(required("gewicht")) ?>> Gewicht</td>
                                        <td><input type="number" name="gewicht" placeholder="gram" class="form-control"
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> Opmerkingen</td>
                                        <td>
                                            <textarea rows="4" cols="50" name="opmerking" form="form"
                                                      placeholder="Opmerking" class="form-control"></textarea>
                                        </td>
                                    </tr>
                                </table>
                                <br>
                                <input type="submit" name="next" value="NEXT" class="btn btn-primary">
                                <br>
                                <br>
                                <?php

                                if ($error1 == true) {
                                    print("Niet alle verplichte velden zijn ingevuld");
                                }
                                ?>

                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php
require_once('/includes/footer.php');
?>

