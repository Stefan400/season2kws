<?php
require_once("/includes/header.php");


//lege variabelen
$melding = "";
$afzenderID = $_GET["afzID"];
$ontvangerID = $_GET["oID"];
$lengte = $_GET["l"];
$breedte = $_GET["b"];
$hoogte = $_GET["h"];
$gewicht = $_GET["g"];
$groen = $_GET["gr"];

if ($groen == 1) {
    $melding = "Uw pakketje wordt zo groen mogelijk bezorgd!";
} else {
    $melding = "Uw pakketje is helaas te groot of te zwaar om met de groene bezorging bezorgd te worden. Uw pakketje wordt bezorgd met de reguliere bezorgservice";
}
if (isset($_POST["stationStation"])) {
    ?>
    <script> window.location.replace("pakketAanmelden3_1.php?afzID=<?php print($afzenderID);?>&oID=<?php print($ontvangerID);?>&l=<?php print($lengte);?>&b=<?php print($breedte);?>&h=<?php print($hoogte);?>&g=<?php print($gewicht);?>&oh=0"); </script> <?php
} elseif (isset($_POST["stationDeur"])) {
    ?>
    <script> window.location.replace("pakketAanmelden3_2.php?afzID=<?php print($afzenderID);?>&oID=<?php print($ontvangerID);?>&l=<?php print($lengte);?>&b=<?php print($breedte);?>&h=<?php print($hoogte);?>&g=<?php print($gewicht);?>&oh=0"); </script> <?php
} elseif (isset($_POST["deurStation"])) {
    ?>
    <script> window.location.replace("pakketAanmelden3_3.php?afzID=<?php print($afzenderID);?>&oID=<?php print($ontvangerID);?>&l=<?php print($lengte);?>&b=<?php print($breedte);?>&h=<?php print($hoogte);?>&g=<?php print($gewicht);?>&oh=0"); </script> <?php
} elseif (isset($_POST["deurDeur"])) {
    ?>
    <script> window.location.replace("pakketAanmelden3_4.php?afzID=<?php print($afzenderID);?>&oID=<?php print($ontvangerID);?>&l=<?php print($lengte);?>&b=<?php print($breedte);?>&h=<?php print($hoogte);?>&g=<?php print($gewicht);?>&oh=0"); </script> <?php
}
?>


<form action="" method="post">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div id="naglowek">
                    <div class="row"> <?php print($melding); ?> </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-md-12 col-xs-12 border">
                            <div class="col-md-3">
                                <input type="submit" name="stationStation" value="Van Station naar Station"
                                       class="btn btn-success btn-lg">
                            </div>
                            <div class="col-md-3">
                                <input type="submit" name="stationDeur" value="Van Station tot Deur"
                                       class="btn btn-success btn-lg">
                            </div>
                            <div class="col-md-3">
                                <input type="submit" name="deurStation" value="Van Deur tot Station"
                                       class="btn btn-success btn-lg">
                            </div>
                            <input type="submit" name="deurDeur" value="Van Deur tot Deur"
                                   class="btn btn-success btn-lg">
                        </div>

                    </div>
                </div>
                <br> <br>
            </div>
        </div>
    </div>
    </div>
</form>

<?php
require_once('/includes/footer.php');
?>

