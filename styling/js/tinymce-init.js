tinymce.init({
    selector: 'textarea',
    height: 300,
    theme: 'modern',
    plugins: [
        'advlist autolink lists link charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
    ],
    toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
    toolbar2: 'preview media | forecolor backcolor emoticons | codesample myimage',
    setup: function (editor) {
        editor.addButton('myimage', {
            text: 'Afbeelding',
            icon: false,
            onclick: function () {
                $('#myModal').modal('show');
            }
        });
    },
    image_advtab: true,
    templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
    ]
});